package base;

import Engine.EngineBase;

/**
 * @build JavaSE 1.8
 * @author CudaSloth
 * Entry point
 */
public class Entry 
{
	public static void main(String[] args) 
	{
		boolean _exit = false;
		EngineBase _e = EngineBase.getInstance();
		EngineBase.setDebug(true);
		
		_e.init();
		
		while(!_exit)
		{
			_e.update(System.currentTimeMillis());	
			_e.draw();
		}
		
	}
}
