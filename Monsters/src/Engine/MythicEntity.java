package Engine;

import Species.Dragon;

public interface MythicEntity 
{
	public void act_MoveExt1(Dragon _target, Dragon _source);
	public void act_MoveExt2(Dragon _t, Dragon _s);
	public void act_MoveExt3(Dragon _t, Dragon _s);
	public void act_MoveExt4(Dragon _t, Dragon _s);
	public void act_MoveExt5(Dragon _t, Dragon _s);
	public void act_MoveExt6(Dragon _t, Dragon _s);
	public void act_MoveExt7(Dragon _t, Dragon _s);
	public void act_MoveExt8(Dragon _t, Dragon _s);
}
