package Engine;
import java.util.ArrayList;
import Species.*;
import MythicTypes.*;

public class DragonFarm 
{
	private static DragonFarm thisInstance;
	
	private int _baseAtk 	= 8;
	private int _baseAtk_sp = 8;
	private int _baseDef 	= 5;
	private int _baseDef_sp = 5;
	private float _baseSpd 	= 0.5f;
	private float _baseSpd_sp	= 0.5f;
	private int _baseAgi 	= 3;
	private int _baseAgi_sp	= 3;
	private int _baseSpi	= 2;
	private int _baseInt	= 2;
	private int _basePer	= 2;
	private int _baseLuc	= 1;
	private int _baseRep 	= 0;
	
	public static DragonFarm getInstance()
	{
		if(thisInstance == null)
		{
			thisInstance = new DragonFarm();
		}
		return thisInstance;
	}
	
	private DragonFarm()
	{
	}
	
	public int getBaseAtk() 	{ return _baseAtk; 		}
	public int getBaseAtkSp() 	{ return _baseAtk_sp; 	}
	public int getBaseDef() 	{ return _baseDef; 		}
	public int getBaseDefSp()	{ return _baseDef_sp; 	}
	public float getBaseSpd() 	{ return _baseSpd; 		}
	public float getBaseSpdSp()	{ return _baseSpd_sp; 	}
	public int getBaseAgi() 	{ return _baseAgi; 		}
	public int getBaseAgiSp()	{ return _baseAgi_sp; 	}
	public int getBaseSpi()		{ return _baseSpi; 		}
	public int getBaseInt()		{ return _baseInt; 		}
	public int getBasePer() 	{ return _basePer; 		}
	public int getBaseLuc()		{ return _baseLuc;	 	}
	public int getBaseRep()		{ return _baseRep; 		}
	
	public ArrayList<Dragon> getLocalSpecies(int _lowerBound, int _upperBound)
	{
		ArrayList<Dragon> _temp = new ArrayList<Dragon>();
		if(EngineBase.getDebug())
			System.out.println("Monster farm generating tiered group: Tier(" + _lowerBound + "," + _upperBound + ")");
		for(int _curr = _lowerBound; _curr <= _upperBound; _curr++)
		{
			if(_curr == 1)
			{
				_temp.add(new Aeloria());
				_temp.add(new Lucida());
				_temp.add(new Pandriff());
				_temp.add(new Baltus());
			}
			if(_curr == 2)
			{
				/*_temp.add(new Ampra());
				_temp.add(new Atlas());
				_temp.add(new Buular());
				_temp.add(new Praetorian());*/
			}
			if(_curr == 3)
			{
				/*_temp.add(new Cendrithofar());
				_temp.add(new Shura());
				_temp.add(new Arceus());
				_temp.add(new Baalthurias());*/
			}
		}
		return _temp;
	}
	
	public Dragon getDragonBySpecies(String _t)
	{
		if(_t == "Aeloria")
			return new Aeloria();
		if(_t == "Lucida")
			return new Lucida();
		if(_t == "Pandriff")
			return new Pandriff();
		if(_t == "Baltus")
			return new Baltus();
		else
			return new ErrorSpecies();
	}
	
	public Dragon getDragon()
	{
		int _q = EngineBase.getInstance().getRandomInt(4);
		if(_q == 0)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aeloria");
			return new Aeloria();
		}
		if(_q == 1)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Lucida");
			return new Lucida();
		}
		if(_q == 2)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Pandriff");
			return new Pandriff();
		}
		if(_q == 3)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Baltus");
			return new Baltus();
		}
		/*if(_q == 4)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Dendroprodus");
			return new Dendroprodus();
		}
		if(_q == 5)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Ekranophus");
			return new Ekranophus();
		}
		if(_q == 6)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Galltean");
			return new Galltean();
		}
		if(_q == 7)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Juurik");
			return new Juurik();
		}
		if(_q == 8)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Praetorian");
			return new Praetorian();
		}
		if(_q == 9)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Shura");
			return new Shura();
		}
		if(_q == 10)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Stunted");
			return new StuntedDragon();
		}
		if(_q == 11)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Trathodor");
			return new Trathodor();
		}
		if(_q == 12)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Ytraenus");
			return new Ytraenus();
		}
		if(_q == 13)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aethertan");
			return new Aethertan();
		}
		if(_q == 14)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Akorptus");
			return new Akorptus();
		}
		if(_q == 15)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Afdraak");
			return new Afdraak();
		}
		if(_q == 16)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Akreptan");
			return new Akreptan();
		}
		if(_q == 17)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aldeberaan");
			return new Aldeberaan();
		}
		if(_q == 18)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aleminna");
			return new Aleminna();
		}
		if(_q == 19)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Ampra");
			return new Ampra();
		}
		if(_q == 20)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aprek");
			return new Aprek();
		}
		if(_q == 21)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Aprotiac");
			return new Aprotiac();
		}
		if(_q == 22)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Arceus");
			return new Arceus();
		}
		if(_q == 23)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Atlas");
			return new Atlas();
		}
		if(_q == 24)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Baalthurias");
			return new Baalthurias();
		}
		if(_q == 25)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Baardaskein");
			return new Baardaskein();
		}
		if(_q == 26)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Barutan");
			return new Barutan();
		}
		if(_q == 27)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Berkan");
			return new Berkan();
		}
		if(_q == 28)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Binkael");
			return new Binkael();
		}
		if(_q == 29)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Ytraenus");
			return new Ytraenus();
		}
		if(_q == 30)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Brakuraak");
			return new Brakuraak();
		}
		if(_q == 31)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Brateos");
			return new Brateos();
		}
		if(_q == 32)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Brikean");
			return new Brikean();
		}
		if(_q == 33)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Buular");
			return new Buular();
		}
		if(_q == 34)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Fandra");
			return new Fandra();
		}
		if(_q == 35)
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Cendrithofar");
			return new Cendrithofar();
		}*/
		else
		{
			if(EngineBase.getDebug())
				System.out.println("Monster farm answering callsite, returning: Error");
			return new ErrorSpecies();
		}
	}
	
	public Mythic getMythic()
	{
		int _q = EngineBase.getInstance().getRandomInt(5);
		if(_q == 0)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Baby");
			return new BabyDragon();
		}
		if(_q == 1)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Young");
			return new YoungDragon();
		}
		if(_q == 2)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Outcast");
			return new OutcastDragon();
		}
		if(_q == 3)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Lesser");
			return new LesserDragon();
		}
		if(_q == 4)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Greater");
			return new GreaterDragon();
		}
		/*if(_q == 5)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Shattered");
			return new ShatteredDragon();
		}
		if(_q == 6)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Spirit");
			return new SpiritDragon();
		}
		if(_q == 7)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Nomad");
			return new NomadDragon();
		}
		if(_q == 8)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Angelic");
			return new AngelicDragon();
		}
		if(_q == 9)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Demonic");
			return new DemonicDragon();
		}
		if(_q == 10)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Legendary");
			return new LegendaryDragon();
		}
		if(_q == 11)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Mythic");
			return new MythicDragon();
		}
		if(_q == 12)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Ancient");
			return new AncientDragon();
		}
		if(_q == 13)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Aged");
			return new AgedDragon();
		}
		if(_q == 14)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Alpha");
			return new AlphaDragon();
		}
		if(_q == 15)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Bountiful");
			return new BountifulDragon();
		}
		if(_q == 16)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Calm");
			return new CalmDragon();
		}
		if(_q == 17)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Devora");
			return new DevoraDragon();
		}
		if(_q == 18)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Dutiful");
			return new DutifulDragon();
		}
		if(_q == 19)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Empowered");
			return new EmpoweredDragon();
		}
		if(_q == 20)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Enraged");
			return new EnragedDragon();
		}
		if(_q == 21)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Ensnared");
			return new EnsnaredDragon();
		}
		if(_q == 22)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Faerae");
			return new FaeraeDragon();
		}
		if(_q == 23)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Fearful");
			return new FearfulDragon();
		}
		if(_q == 24)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Friendly");
			return new FriendlyDragon();
		}
		if(_q == 25)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Ancient");
			return new AncientDragon();
		}
		if(_q == 26)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Genetically Engineered");
			return new GeneSplicedDragon();
		}
		if(_q == 27)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Glorious");
			return new GloriousDragon();
		}
		
		if(_q == 28)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Herded");
			return new HerdedDragon();
		}
		if(_q == 29)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Insightful");
			return new InsightfulDragon();
		}
		if(_q == 30)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Intelligent");
			return new IntelligentDragon();
		}
		if(_q == 31)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Krilteka");
			return new KriltekaDragon();
		}
		if(_q == 32)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Limping");
			return new LimpingDragon();
		}
		if(_q == 33)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Lyrael");
			return new LyraelDragon();
		}
		if(_q == 34)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Persecuted");
			return new PersecutedDragon();
		}
		if(_q == 35)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Promiscuous");
			return new PromiscuousDragon();
		}
		if(_q == 36)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Quiet");
			return new QuietDragon();
		}
		if(_q == 37)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Roaming");
			return new RoamingDragon();
		}
		if(_q == 38)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Terrestrial");
			return new TerrestrialDragon();
		}
		if(_q == 39)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Territorial");
			return new TerritorialDragon();
		}
		if(_q == 40)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Timid");
			return new TimidDragon();
		}
		if(_q == 41)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Tortured");
			return new TorturedDragon();
		}
		if(_q == 42)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Underling");
			return new UnderlingDragon();
		}
		if(_q == 43)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Usurped");
			return new UsurpedDragon();
		}
		if(_q == 44)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Wisened");
			return new WisenedDragon();
		}
		if(_q == 45)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Zaal");
			return new ZaalDragon();
		}
		if(_q == 46)
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Mechanical");
			return new MechanicalDragon();
		}*/
		else
		{
			if(EngineBase.getDebug())
				System.out.println("Mythic farm answering callsite, returning: Error");
			return new ErrorMythic();
		}
	}
}
