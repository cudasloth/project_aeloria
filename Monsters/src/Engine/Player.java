package Engine;
import java.util.ArrayList;

import Species.Dragon;

/**
 * 
 * @author CudaSloth
 * Contains the user stats and inventory
 */
public class Player 
{
	private ArrayList<Dragon> _party;
	private ArrayList<Dragon> _partyActive;
	
	public Player()
	{	
		_party = new ArrayList<Dragon>();
		_partyActive = new ArrayList<Dragon>();
	}
	
	public void init()
	{
		int i = 0;
		int a = 0;
		
		for(int j = 0; j < 6; ++j)
		{
			this.addDragon(DragonFarm.getInstance().getDragon());
		}
		
		for(int j = 0; j < _party.size(); ++j)
		{
			_party.get(i).generateLevelledStats(a + 1);
			++i;
			++a;
			if(a > 10)
			{
				a = 0;
			}
		}
		
		for(int j = 0; j < 3; ++j)
		{
			_partyActive.add(_party.get(j));
		}
	}
	
	public void update()
	{	
		_partyActive.get(0).update();
		_partyActive.get(1).update();
		_partyActive.get(2).update();
	}
	
	public void draw()
	{
		/*for(Dragon d : _party)
		{
			System.out.println("Party Member: " + d.getTitle());
			System.out.println("Stats Atk: " + d.getAtk());
			System.out.println("Stats Def: " + d.getDef());
			System.out.println("Stats Spd: " + d.getSpd());
			System.out.println("Stats Agi: " + d.getAgi());
			System.out.println("Stats Sp Atk: " + d.getSpAtk());
			System.out.println("Stats Sp Def: " + d.getSpDef());
			System.out.println("Stats Sp Spd: " + d.getSpSpd());
			System.out.println("Stats Sp Agi: " + d.getSpAgi());
			System.out.println("Stats Spi: " + d.getSpi());
			System.out.println("Stats Per: " + d.getPer());
			System.out.println("Stats Int: " + d.getInt());
			System.out.println("Stats Luck: " + d.getLuck());
			System.out.println("Stats Rep: " + d.getRep());
		}*/
	}
	
	public Dragon getActiveMember(int _i)
	{
		return _partyActive.get(_i);
	}
	
	public boolean addDragon(Dragon _m)
	{
		if(_party.size() < 100)
		{
			_party.add(_m);
			if(EngineBase.getDebug())
				System.out.println("Added dragon of type: " + _m.getType());
		}
		else
		{
			if(EngineBase.getDebug())
				System.out.println("Could not add dragon, party full!");
			return false;
		}
		return true;
	}
	
	public Dragon getDragon(int _i)
	{
		return _party.get(_i);
	}
	
	public ArrayList<Dragon> getParty()
	{
		return _party;
	}
}
