package Engine;

import java.util.ArrayList;

import Zoning.Beach;
import Zoning.Forest;
import Zoning.Grass;
import Zoning.Zone;

public class TerrainEngine 
{
	private static TerrainEngine thisInstance;
	private ArrayList<Zone> _map;
	
	private TerrainEngine()
	{
		_map = new ArrayList<Zone>();
	}
	
	public static TerrainEngine getInstance()
	{
		if(thisInstance == null)
		{
			thisInstance = new TerrainEngine();
		}
		return thisInstance;
	}
	
	public void init()
	{
		this.addZone(new Grass(1,1));
		this.addZone(new Beach(1,3));
		this.addZone(new Forest(2,3));
	}
	
	public void addZone(Zone _z)
	{
		_map.add(_z);
	}
	
	public Zone getZone(int _i)
	{
		return _map.get(_i);
	}
}
