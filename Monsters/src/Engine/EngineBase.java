package Engine;

import Species.AbilityLibrary;

import java.util.Random;


/**
 * 
 * @author CudaSloth
 * Engine for monsters, controls the base mvc structure
 */


public class EngineBase 
{
	private static EngineBase thisInstance;
	private Player thisPlayer;
	private TerrainEngine thisTerrain;
	private PeripheralEngine thisPeripheral;
	private static boolean _debug;
	long currTime, lastTime;
	private Random _r;
	
	public static EngineBase getInstance()
	{
		if(thisInstance == null)
		{
			thisInstance = new EngineBase();
		}
		return thisInstance;
	}
	
	public static boolean getDebug() { return _debug; }	
	public static void setDebug(boolean _b) { _debug = _b; }
	
	private EngineBase()
	{
		DragonFarm.getInstance();
		thisPlayer = new Player();
		thisTerrain = TerrainEngine.getInstance();
		thisPeripheral = PeripheralEngine.getInstance();
		AbilityLibrary.getInstance();
		_r = new Random();
	} 
	
	public Player getPlayer()
	{
		return thisPlayer;
	}
	
	public int getRandomInt(int _range)
	{
		return _r.nextInt(_range);
	}
	
	public void init()
	{
		thisPlayer.init();
		thisTerrain.init();
		thisPeripheral.init();
	}
	
	public void update(long _sysTime)
	{		
		currTime = _sysTime;
		
		if((currTime-lastTime) >= 10)
		{
			thisPlayer.update();
			thisPeripheral.update();
			
			lastTime = currTime;
		}
		
		
		
		
	}
	
	public void draw()
	{
		thisPlayer.draw();
		thisPeripheral.draw();
		/*thisPlayer.update();
		int _a = 0;
		for(Dragon e : thisPlayer.getParty())
		{
			if(e.getSpecies() == "Aeloria")
			{
				System.out.println("Type call on: " + _a);
				((Aeloria) e).FireBall(e, e);
			}
			if(e.getSpecies() == "Arceus")
			{
				System.out.println("Type call on: " + _a);
				((Arceus) e).Blizzard(e, e);
			}
			if(e.getSpecies() == "Ekranophus")
			{
				System.out.println("Type call on: " + _a);
				((Ekranophus) e).AncientVoices(e, e);
			}
			if(e.getSpecies() == "Afdraak")
			{
				System.out.println("Type call on: " + _a);
				((Afdraak) e).ArcaneChannel(e, e);
			}
			++_a;
		}*/
		//thisPlayer.draw();
	}
}
