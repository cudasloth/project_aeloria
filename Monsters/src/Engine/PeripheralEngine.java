package Engine;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import Species.Dragon;
import Zoning.Zone;

public class PeripheralEngine implements ActionListener
{
	private static PeripheralEngine thisInstance;
	
	private JFrame _main;													//Active frames
	private JFrame _battle;
	private JFrame _zones;
	
	private JPanel _mainPanel;												//Default container Panels
	private JPanel _battlePanel;
	private JPanel _zonePanel;
	
	private JPanel _statPanelEnemy;											//Main container subs
	private JPanel _statPanelPlayer;
	private JPanel _imgPanelEnemy;
	private JPanel _imgPanelPlayer;
	private JPanel _abiPanelEnemy;
	private JPanel _abiPanelPlayer;
	
	private JPanel _partyPanel;												//Battle subs
	private JPanel _enemyPanel;
	
	private JPanel _party1, _party2, _party3;
	private JPanel _enemy1, _enemy2, _enemy3;
	
	private JButton _trial1;
	private JButton _trial2;
	private JButton _trial3;
	
	private JProgressBar _hp1, _hp2, _hp3;
	private JProgressBar _mp1, _mp2, _mp3;
	private JProgressBar _atb1, _atb2, _atb3;
	
	private JProgressBar _hp4, _hp5, _hp6;
	private JProgressBar _mp4, _mp5, _mp6;
	private JProgressBar _atb4, _atb5, _atb6;
	
	private ArrayList<JButton> _moveSet;
	private Dragon _e1, _e2, _e3, _d;
	private Dragon _p1, _p2, _p3, _p;
	
	private PeripheralEngine()
	{
		_main = new JFrame("Monsters");	
		_battle = new JFrame("Battle Window");
		_zones = new JFrame("Zones");
		
		_mainPanel = new JPanel(new GridLayout(2,1));
		_battlePanel = new JPanel(new GridLayout(1,2));
		_zonePanel = new JPanel(new GridLayout(1,3));
		
		_statPanelEnemy = new JPanel(new GridLayout(15,2));
		_statPanelPlayer = new JPanel(new GridLayout(15,2));
		_abiPanelEnemy = new JPanel(new GridLayout(4,7));
		_abiPanelPlayer = new JPanel(new GridLayout(4,7));
		_imgPanelEnemy = new JPanel();
		_imgPanelPlayer = new JPanel();
		
		_partyPanel = new JPanel(new GridLayout(4,1));
		_enemyPanel = new JPanel(new GridLayout(4,1));
		
		_party1 = new JPanel(new GridLayout(7,1));
		_party2 = new JPanel(new GridLayout(7,1));
		_party3 = new JPanel(new GridLayout(7,1));
		_enemy1 = new JPanel(new GridLayout(7,1));
		_enemy2 = new JPanel(new GridLayout(7,1));
		_enemy3 = new JPanel(new GridLayout(7,1));
		
		_moveSet = new ArrayList<JButton>();
	}
	
	public static PeripheralEngine getInstance()
	{
		if(thisInstance == null)
		{
			thisInstance = new PeripheralEngine();
		}
		return thisInstance;
	}
	
	public void init()
	{
		_e1 = DragonFarm.getInstance().getDragon();
		_e1.generateLevelledStats(EngineBase.getInstance().getRandomInt(10));
		_e2 = DragonFarm.getInstance().getDragon();
		_e2.generateLevelledStats(EngineBase.getInstance().getRandomInt(10));
		_e3 = DragonFarm.getInstance().getDragon();
		_e3.generateLevelledStats(EngineBase.getInstance().getRandomInt(10));
		
		_p1 = EngineBase.getInstance().getPlayer().getActiveMember(0);
		_p2 = EngineBase.getInstance().getPlayer().getActiveMember(1);
		_p3 = EngineBase.getInstance().getPlayer().getActiveMember(2);
		
		_p = _p1;
		_d = _e1;
		
		//Player
		for(String s : _p.getAbilityHeaders())
		{
			JButton _j = new JButton(s);
			_j.addActionListener(this);
			_moveSet.add(_j);
			_abiPanelPlayer.add(_j);
		}
		
		for(String s : _p.getExtendedHeaders())
		{
			JButton _j = new JButton(s);
			_j.addActionListener(this);
			_moveSet.add(_j);
			_abiPanelPlayer.add(_j);
		}
		
		for(String s : _p.getRoleHeaders())
		{
			JButton _j = new JButton(s);
			_j.addActionListener(this);
			_moveSet.add(_j);
			_abiPanelPlayer.add(_j);
		}
		
		_statPanelPlayer.add(new JLabel("HP"));
		_statPanelPlayer.add(new JLabel("" + _p.getHealth()));
		_statPanelPlayer.add(new JLabel("MP"));
		_statPanelPlayer.add(new JLabel("" + _p.getMana()));
		_statPanelPlayer.add(new JLabel("Role"));
		_statPanelPlayer.add(new JLabel("" + _p.getRole()));
		_statPanelPlayer.add(new JLabel("Attack"));
		_statPanelPlayer.add(new JLabel("" + _p.getAtk()));
		_statPanelPlayer.add(new JLabel("Special Attack"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpAtk()));
		_statPanelPlayer.add(new JLabel("Defence"));
		_statPanelPlayer.add(new JLabel("" + _p.getDef()));
		_statPanelPlayer.add(new JLabel("Special Defence"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpDef()));
		_statPanelPlayer.add(new JLabel("Speed"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpd()));
		_statPanelPlayer.add(new JLabel("Special Speed"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpSpd()));
		_statPanelPlayer.add(new JLabel("Agility"));
		_statPanelPlayer.add(new JLabel("" + _p.getAgi()));
		_statPanelPlayer.add(new JLabel("Special Agility"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpAgi()));
		_statPanelPlayer.add(new JLabel("Spirit"));
		_statPanelPlayer.add(new JLabel("" + _p.getSpi()));
		_statPanelPlayer.add(new JLabel("Intelligence"));
		_statPanelPlayer.add(new JLabel("" + _p.getInt()));
		_statPanelPlayer.add(new JLabel("Perception"));
		_statPanelPlayer.add(new JLabel("" + _p.getPer()));
		_statPanelPlayer.add(new JLabel("Luck"));
		_statPanelPlayer.add(new JLabel("" + _p.getLuck()));
		
		_imgPanelPlayer.add(new JLabel(_p.getTitle()));
		
		//Enemy
		for(String s : _d.getAbilityHeaders())
		{
			JLabel _j = new JLabel(s);
			_abiPanelEnemy.add(_j);
		}
		
		for(String s : _d.getExtendedHeaders())
		{
			JLabel _j = new JLabel(s);
			_abiPanelEnemy.add(_j);
		}
		
		for(String s : _d.getRoleHeaders())
		{
			JLabel _j = new JLabel(s);
			_abiPanelEnemy.add(_j);
		}
		
		_statPanelEnemy.add(new JLabel("HP"));
		_statPanelEnemy.add(new JLabel("" + _d.getHealth()));
		_statPanelEnemy.add(new JLabel("MP"));
		_statPanelEnemy.add(new JLabel("" + _d.getMana()));
		_statPanelEnemy.add(new JLabel("Role"));
		_statPanelEnemy.add(new JLabel("" + _d.getRole()));
		_statPanelEnemy.add(new JLabel("Attack"));
		_statPanelEnemy.add(new JLabel("" + _d.getAtk()));
		_statPanelEnemy.add(new JLabel("Special Attack"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpAtk()));
		_statPanelEnemy.add(new JLabel("Defence"));
		_statPanelEnemy.add(new JLabel("" + _d.getDef()));
		_statPanelEnemy.add(new JLabel("Special Defence"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpDef()));
		_statPanelEnemy.add(new JLabel("Speed"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpd()));
		_statPanelEnemy.add(new JLabel("Special Speed"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpSpd()));
		_statPanelEnemy.add(new JLabel("Agility"));
		_statPanelEnemy.add(new JLabel("" + _d.getAgi()));
		_statPanelEnemy.add(new JLabel("Special Agility"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpAgi()));
		_statPanelEnemy.add(new JLabel("Spirit"));
		_statPanelEnemy.add(new JLabel("" + _d.getSpi()));
		_statPanelEnemy.add(new JLabel("Intelligence"));
		_statPanelEnemy.add(new JLabel("" + _d.getInt()));
		_statPanelEnemy.add(new JLabel("Perception"));
		_statPanelEnemy.add(new JLabel("" + _d.getPer()));
		_statPanelEnemy.add(new JLabel("Luck"));
		_statPanelEnemy.add(new JLabel("" + _d.getLuck()));
		
		_imgPanelEnemy.add(new JLabel(_d.getTitle()));
		
		//Main Panel		
		_mainPanel.add(_abiPanelEnemy);
		_mainPanel.add(_statPanelEnemy);
		_mainPanel.add(_imgPanelEnemy);
		_mainPanel.add(_imgPanelPlayer);
		_mainPanel.add(_statPanelPlayer);
		_mainPanel.add(_abiPanelPlayer);
		
		//Battle Panel
		_hp1 = new JProgressBar(0, _p1.getHealth());
		_hp2 = new JProgressBar(0, _p2.getHealth());
		_hp3 = new JProgressBar(0, _p3.getHealth());
		_mp1 = new JProgressBar(0, _p1.getMana());
		_mp2 = new JProgressBar(0, _p2.getMana());
		_mp3 = new JProgressBar(0, _p3.getMana());
		_atb1 = new JProgressBar(0, (int)_p1.getATBMax());
		_atb2 = new JProgressBar(0, (int)_p2.getATBMax());
		_atb3 = new JProgressBar(0, (int)_p3.getATBMax());
		
		_hp4 = new JProgressBar(0, _e1.getHealth());
		_hp5 = new JProgressBar(0, _e2.getHealth());
		_hp6 = new JProgressBar(0, _e3.getHealth());
		_mp4 = new JProgressBar(0, _e1.getMana());
		_mp5 = new JProgressBar(0, _e2.getMana());
		_mp6 = new JProgressBar(0, _e3.getMana());
		_atb4 = new JProgressBar(0, (int)_e1.getATBMax());
		_atb5 = new JProgressBar(0, (int)_e2.getATBMax());
		_atb6 = new JProgressBar(0, (int)_e3.getATBMax());
		
		System.out.println("p1 max atb: " + _p1.getATBMax());
		System.out.println("p2 max atb: " + _p2.getATBMax());
		System.out.println("p3 max atb: " + _p3.getATBMax());
		
		_party1.add(new JLabel(_p1.getTitle()));
		_party1.add(new JLabel("HP"));
		_party1.add(_hp1);
		_party1.add(new JLabel("MP"));
		_party1.add(_mp1);
		_party1.add(new JLabel("ATB"));
		_party1.add(_atb1);
		_party2.add(new JLabel(_p2.getTitle()));
		_party2.add(new JLabel("HP"));
		_party2.add(_hp2);
		_party2.add(new JLabel("MP"));
		_party2.add(_mp2);
		_party2.add(new JLabel("ATB"));
		_party2.add(_atb2);
		_party3.add(new JLabel(_p3.getTitle()));
		_party3.add(new JLabel("HP"));
		_party3.add(_hp3);
		_party3.add(new JLabel("MP"));
		_party3.add(_mp3);
		_party3.add(new JLabel("ATB"));
		_party3.add(_atb3);
		
		_enemy1.add(new JLabel(_e1.getTitle()));
		_enemy1.add(new JLabel("HP"));
		_enemy1.add(_hp4);
		_enemy1.add(new JLabel("MP"));
		_enemy1.add(_mp4);
		_enemy1.add(new JLabel("ATB"));
		_enemy1.add(_atb4);
		_enemy2.add(new JLabel(_e2.getTitle()));
		_enemy2.add(new JLabel("HP"));
		_enemy2.add(_hp5);
		_enemy2.add(new JLabel("MP"));
		_enemy2.add(_mp5);
		_enemy2.add(new JLabel("ATB"));
		_enemy2.add(_atb5);
		_enemy3.add(new JLabel(_e3.getTitle()));
		_enemy3.add(new JLabel("HP"));
		_enemy3.add(_hp6);
		_enemy3.add(new JLabel("MP"));
		_enemy3.add(_mp6);
		_enemy3.add(new JLabel("ATB"));
		_enemy3.add(_atb6);
		
		_partyPanel.add(new JLabel("Party"));
		_partyPanel.add(_party1);
		_partyPanel.add(_party2);
		_partyPanel.add(_party3);
		_enemyPanel.add(new JLabel("Enemies"));
		_enemyPanel.add(_enemy1);
		_enemyPanel.add(_enemy2);
		_enemyPanel.add(_enemy3);
		
		_battlePanel.add(_partyPanel);
		_battlePanel.add(_enemyPanel);
		
		//Zone Panel
		_trial1 = new JButton(TerrainEngine.getInstance().getZone(0).getType());
		_trial2 = new JButton(TerrainEngine.getInstance().getZone(1).getType());
		_trial3 = new JButton(TerrainEngine.getInstance().getZone(2).getType());
		_trial1.addActionListener(PeripheralEngine.getInstance());
		_trial2.addActionListener(PeripheralEngine.getInstance());
		_trial3.addActionListener(PeripheralEngine.getInstance());
		
		_zonePanel.add(_trial1);
		_zonePanel.add(_trial2);
		_zonePanel.add(_trial3);
		
		_main.add(_mainPanel);
		_battle.add(_battlePanel);
		_zones.add(_zonePanel);
		
		_main.pack();
		_main.setVisible(true);
		_main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		_battle.pack();
		_battle.setVisible(true);
		_battle.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		_zones.pack();
		_zones.setVisible(true);
		_zones.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void update()
	{
		_p1 = EngineBase.getInstance().getPlayer().getActiveMember(0);
		_p2 = EngineBase.getInstance().getPlayer().getActiveMember(1);
		_p3 = EngineBase.getInstance().getPlayer().getActiveMember(2);
		
		_e1.update();
		_e2.update();
		_e3.update();
		
		if(_e1.getReady())
		{
			_e1.act_DoAutomated();
			_e1.zeroATB();
		}
		if(_e2.getReady())
		{
			_e2.act_DoAutomated();
			_e2.zeroATB();
		}
		if(_e3.getReady())
		{
			_e3.act_DoAutomated();
			_e3.zeroATB();
		}
	}
	
	public void draw()
	{
		_hp1.setValue(_p1.getHealth());
		_hp2.setValue(_p2.getHealth());
		_hp3.setValue(_p3.getHealth());
		
		_mp1.setValue(_p1.getMana());
		_mp2.setValue(_p2.getMana());
		_mp3.setValue(_p3.getMana());
		
		_atb1.setValue((int)_p1.getATB());
		_atb2.setValue((int)_p2.getATB());
		_atb3.setValue((int)_p3.getATB());
		
		_hp4.setValue(_e1.getHealth());
		_hp5.setValue(_e2.getHealth());
		_hp6.setValue(_e3.getHealth());
		
		_mp4.setValue(_e1.getMana());
		_mp5.setValue(_e2.getMana());
		_mp6.setValue(_e3.getMana());
		
		_atb4.setValue((int)_e1.getATB());
		_atb5.setValue((int)_e2.getATB());
		_atb6.setValue((int)_e3.getATB());
	}
	
	@Override
	public void actionPerformed(ActionEvent _ev) 
	{
		ap1(_ev);
		ap2(_ev);
	}
	
	public void ap1(ActionEvent _e)
	{
		Zone _temp;
		if(_e.getSource() == _trial1)
		{
			_temp = TerrainEngine.getInstance().getZone(0);
			System.out.println("Zone: " + TerrainEngine.getInstance().getZone(0).getType());
			System.out.println("Available Species: " + _temp.getPopSize());
			for(int i = 0; i < _temp.getPopSize(); ++i)
			{	
				System.out.println("Species " + (i + 1) + ": " + _temp.getHeading(i));
			}
		}
		if(_e.getSource() == _trial2)
		{
			_temp = TerrainEngine.getInstance().getZone(1);
			System.out.println("Zone: " + TerrainEngine.getInstance().getZone(1).getType());
			System.out.println("Available Species: " + _temp.getPopSize());
			for(int i = 0; i < _temp.getPopSize(); ++i)
			{	
				System.out.println("Species " + (i + 1) + ": " + _temp.getHeading(i));
			}
		}
		if(_e.getSource() == _trial3)
		{
			_temp = TerrainEngine.getInstance().getZone(2);
			System.out.println("Zone: " + TerrainEngine.getInstance().getZone(2).getType());
			System.out.println("Available Species: " + _temp.getPopSize());
			for(int i = 0; i < _temp.getPopSize(); ++i)
			{	
				System.out.println("Species " + (i + 1) + ": " + _temp.getHeading(i));
			}
		}
	}

	public void ap2(ActionEvent _e)
	{
		if(_e.getSource() == _moveSet.get(0))
			_p.act_Move1();
		if(_e.getSource() == _moveSet.get(1))
			_p.act_Move2();
		if(_e.getSource() == _moveSet.get(2))
			_p.act_Move3();
		if(_e.getSource() == _moveSet.get(3))
			_p.act_Move4();
		if(_e.getSource() == _moveSet.get(4))
			_p.act_Move5();
		if(_e.getSource() == _moveSet.get(5))
			_p.act_Move6();
		if(_e.getSource() == _moveSet.get(6))
			_p.act_Move7();
		if(_e.getSource() == _moveSet.get(7))
			_p.act_Move8();
		if(_e.getSource() == _moveSet.get(8))
			_p.act_Move9();
		if(_e.getSource() == _moveSet.get(9))
			_p.act_Move10();
		if(_e.getSource() == _moveSet.get(10))
			_p.act_Move11();
		if(_e.getSource() == _moveSet.get(11))
			_p.act_Move12();
		
		if(_e.getSource() == _moveSet.get(12))
			_p.act_MoveExt1();
		if(_e.getSource() == _moveSet.get(13))
			_p.act_MoveExt2();
		if(_e.getSource() == _moveSet.get(14))
			_p.act_MoveExt3();
		if(_e.getSource() == _moveSet.get(15))
			_p.act_MoveExt4();
		if(_e.getSource() == _moveSet.get(16))
			_p.act_MoveExt5();
		if(_e.getSource() == _moveSet.get(17))
			_p.act_MoveExt6();
		if(_e.getSource() == _moveSet.get(18))
			_p.act_MoveExt7();
		if(_e.getSource() == _moveSet.get(19))
			_p.act_MoveExt8();
		
		if(_e.getSource() == _moveSet.get(20))
			_p.act_MoveRoleSelect(0);
		if(_e.getSource() == _moveSet.get(21))
			_p.act_MoveRoleSelect(1);
		if(_e.getSource() == _moveSet.get(22))
			_p.act_MoveRoleSelect(2);
		if(_e.getSource() == _moveSet.get(23))
			_p.act_MoveRoleSelect(3);
		if(_e.getSource() == _moveSet.get(24))
			_p.act_MoveRoleSelect(4);
		if(_e.getSource() == _moveSet.get(25))
			_p.act_MoveRoleSelect(5);
		if(_e.getSource() == _moveSet.get(26))
			_p.act_MoveRoleSelect(6);
		if(_e.getSource() == _moveSet.get(27))
			_p.act_MoveRoleSelect(7);
	}
}
