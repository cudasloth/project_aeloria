package Engine;

public interface BattleEntity 
{
	public void act_Move1();
	public void act_Move2();
	public void act_Move3();
	public void act_Move4();
	public void act_Move5();
	public void act_Move6();
	public void act_Move7();
	public void act_Move8();
	public void act_Move9();
	public void act_Move10();
	public void act_Move11();
	public void act_Move12();
	
	public void act_DoAutomated();
	public void act_MoveRoleSelect(int _i);
}
