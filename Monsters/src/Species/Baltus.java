package Species;

import ElementalType.Terran;
import Engine.EngineBase;
import Roles.Tank;

public class Baltus extends Dragon implements Terran, Tank
{
	public Baltus()
	{
		_elementalType = _baseType;
		_role = _baseRole;
		_moves = _abilityHeaders;
		
		_mythicType = Engine.DragonFarm.getInstance().getMythic();
		_species = "Baltus";
	}

	@Override
	public void act_Move1() {
		AbilityLibrary.getInstance().Rumble(this, this);
		
	}

	@Override
	public void act_Move2() {
		AbilityLibrary.getInstance().Crack(this, this);
		
	}

	@Override
	public void act_Move3() {
		AbilityLibrary.getInstance().RockFall(this, this);
		
	}

	@Override
	public void act_Move4() {
		AbilityLibrary.getInstance().Barricade(this, this);
		
	}

	@Override
	public void act_Move5() {
		AbilityLibrary.getInstance().EarthSwallow(this, this);
		
	}

	@Override
	public void act_Move6() {
		AbilityLibrary.getInstance().Earthquake(this, this);
		
	}

	@Override
	public void act_Move7() {
		AbilityLibrary.getInstance().RockStorm(this, this);
		
	}

	@Override
	public void act_Move8() {
		AbilityLibrary.getInstance().Subsidance(this, this);
		
	}

	@Override
	public void act_Move9() {
		AbilityLibrary.getInstance().FaultLine(this, this);
		
	}

	@Override
	public void act_Move10() {
		AbilityLibrary.getInstance().Landmass(this, this);
		
	}

	@Override
	public void act_Move11() {
		AbilityLibrary.getInstance().PlanetaryBreak(this, this);
		
	}

	@Override
	public void act_Move12() {
		AbilityLibrary.getInstance().EchoesOfTerra(this, this);
		
	}

	@Override
	public void act_MoveRoleSelect(int _i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_DoAutomated() 
	{
		int _r = EngineBase.getInstance().getRandomInt(28);
		
		if(_r == 0)
			this.act_Move1();
		if(_r == 1)
			this.act_Move2();
		if(_r == 2)
			this.act_Move3();
		if(_r == 3)
			this.act_Move4();
		if(_r == 4)
			this.act_Move5();
		if(_r == 5)
			this.act_Move6();
		if(_r == 6)
			this.act_Move7();
		if(_r == 7)
			this.act_Move8();
		if(_r == 8)
			this.act_Move9();
		if(_r == 9)
			this.act_Move10();
		if(_r == 10)
			this.act_Move11();
		if(_r == 11)
			this.act_Move12();
		if(_r == 12)
			this.act_MoveExt1();
		if(_r == 13)
			this.act_MoveExt2();
		if(_r == 14)
			this.act_MoveExt3();
		if(_r == 15)
			this.act_MoveExt4();
		if(_r == 16)
			this.act_MoveExt5();
		if(_r == 17)
			this.act_MoveExt6();
		if(_r == 18)
			this.act_MoveExt7();
		if(_r == 19)
			this.act_MoveExt8();
		if(_r == 20)
			this.act_MoveRoleSelect(0);
		if(_r == 21)
			this.act_MoveRoleSelect(1);
		if(_r == 22)
			this.act_MoveRoleSelect(2);
		if(_r == 23)
			this.act_MoveRoleSelect(3);
		if(_r == 24)
			this.act_MoveRoleSelect(4);
		if(_r == 25)
			this.act_MoveRoleSelect(5);
		if(_r == 26)
			this.act_MoveRoleSelect(6);
		if(_r == 27)
			this.act_MoveRoleSelect(7);
	}
}
