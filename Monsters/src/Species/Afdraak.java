package Species;

import ElementalType.Devora;

public class Afdraak extends Dragon implements Devora
{
	public Afdraak()
	{
		_elementalType = _baseType;
		_mythicType = Engine.DragonFarm.getInstance().getMythic();
		_species = "Afdraak";
	}

	@Override
	public void ArcaneChannel(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().ArcaneChannel(_t, _s);
		
	}

	@Override
	public void TribalCall(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ArmorBurst(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void EarthShatter(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Sandstorm(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void WarringCall(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Thrash(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Embolden(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void AmiriaDevora(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void SpiritualHaunting(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Eviscerate(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void DevoricSummoning(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move5() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move6() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move7() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move8() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move9() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move10() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move11() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move12() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt5() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt6() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt7() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt8() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveRoleSelect(int _i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_DoAutomated() {
		// TODO Auto-generated method stub
		
	}

}
