package Species;

import Engine.BattleEntity;
import MythicTypes.Mythic;

/**
 * 
 * @author CudaSloth
 * Monster parent class
 */
public abstract class Dragon implements BattleEntity
{
	protected String _elementalType;		//Base type as string
	protected Mythic _mythicType;			//Mythic type
	protected String _species;				//Base species
	protected String _role;					//Base role in team
	protected String[] _moves;
	protected String[] _roleHeaders = {"", "", "", "", "", "", "", ""};
	protected int[] _roleIDs = {-1, -1, -1, -1, -1, -1, -1, -1};
	
	protected int _atk;						//Base stats
	protected int _atk_sp;
	protected int _def;
	protected int _def_sp;
	protected float _spd;
	protected float _spd_sp;
	protected int _agi;
	protected int _agi_sp;
	protected int _spi;
	protected int _int;
	protected int _per;
	protected int _luc;
	protected int _rep;
	
	protected int _hp;						//Dynamic stats
	protected int _mp;
	protected int _specCharge;
	protected int _lvl = 1;
	protected float _atb = 0.0f;
	protected float _atbMax = 0.0f;
	protected boolean _isReady = false;
	
	public void update()
	{
		if(_atb < getATBMax())
		{
			_atb += _spd/10;
			//System.out.println(getTitle() + " atb: " + getATB());
		}
		else
		{
			//_atb = 0.0f;
			_isReady = true;
		}
	}
	
	public void draw()
	{
		//TODO draw code for render
	}
	
	public void incrementBaseStats()
	{
		_lvl++;
		_atk 		+= 		Engine.DragonFarm.getInstance().getBaseAtk()		+		_mythicType.getBonusAtk();
		_atk_sp 	+= 		Engine.DragonFarm.getInstance().getBaseAtkSp()		+		_mythicType.getBonusAtkSp();
		_def 		+= 		Engine.DragonFarm.getInstance().getBaseDef()		+		_mythicType.getBonusDef();
		_def_sp 	+= 		Engine.DragonFarm.getInstance().getBaseDefSp()		+		_mythicType.getBonusDefSp();
		_spd 		+= 		Engine.DragonFarm.getInstance().getBaseSpd()		+		_mythicType.getBonusSpd();
		_spd_sp 	+= 		Engine.DragonFarm.getInstance().getBaseSpdSp()		+		_mythicType.getBonusSpdSp();
		_agi 		+= 		Engine.DragonFarm.getInstance().getBaseAgi()		+		_mythicType.getBonusAgi();
		_agi_sp 	+= 		Engine.DragonFarm.getInstance().getBaseAgiSp()		+		_mythicType.getBonusAgiSp();
		_spi 		+= 		Engine.DragonFarm.getInstance().getBaseSpi()		+		_mythicType.getBonusSpi();
		_int 		+= 		Engine.DragonFarm.getInstance().getBaseInt()		+		_mythicType.getBonusInt();
		_per 		+= 		Engine.DragonFarm.getInstance().getBasePer()		+		_mythicType.getBonusPer();
		_luc 		+= 		Engine.DragonFarm.getInstance().getBaseLuc()		+		_mythicType.getBonusLuc();
		_rep		+= 		Engine.DragonFarm.getInstance().getBaseRep()		+		_mythicType.getBonusRep();
		_hp			+=		(8 + _spi)											+		(3 + _mythicType.getBonusSpi());
		_mp			+=		(2 + _int)											+		(1 + _mythicType.getBonusInt());
	}
	
	public void generateLevelledStats(int _l)
	{
		_lvl = _l;
		_atk 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseAtk())		+		(_l * _mythicType.getBonusAtk());
		_atk_sp 	+= 		(_l * Engine.DragonFarm.getInstance().getBaseAtkSp())	+		(_l * _mythicType.getBonusAtkSp());
		_def 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseDef())		+		(_l * _mythicType.getBonusDef());
		_def_sp 	+= 		(_l * Engine.DragonFarm.getInstance().getBaseDefSp())	+		(_l * _mythicType.getBonusDefSp());
		_spd 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseSpd())		+		(_l * _mythicType.getBonusSpd());
		_spd_sp 	+= 		(_l * Engine.DragonFarm.getInstance().getBaseSpdSp())	+		(_l * _mythicType.getBonusSpdSp());
		_agi 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseAgi())		+		(_l * _mythicType.getBonusAgi());
		_agi_sp 	+= 		(_l * Engine.DragonFarm.getInstance().getBaseAgiSp())	+		(_l * _mythicType.getBonusAgiSp());
		_spi 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseSpi())		+		(_l * _mythicType.getBonusSpi());
		_int 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseInt())		+		(_l * _mythicType.getBonusInt());
		_per 		+= 		(_l * Engine.DragonFarm.getInstance().getBasePer())		+		(_l * _mythicType.getBonusPer());
		_luc 		+= 		(_l * Engine.DragonFarm.getInstance().getBaseLuc())		+		(_l * _mythicType.getBonusLuc());
		_rep		+= 		(_l * Engine.DragonFarm.getInstance().getBaseRep())		+		(_l * _mythicType.getBonusRep());
		_hp			+=		(_l * (8 + _spi))										+		(_l * (3 + _mythicType.getBonusSpi()));
		_mp			+=		(_l * (2 + _int))										+		(_l * (1 + _mythicType.getBonusInt()));
	}
	
	public String getType()
	{
		return _elementalType;
	}
	
	public String getMythic()
	{
		return _mythicType.getBase();
	}
	
	public String getSpecies()
	{
		return _species;
	}
	
	public String getRole()
	{
		return _role;
	}
	
	public int getAtk()
	{
		return _atk;
	}
	
	public int getSpAtk()
	{
		return _atk_sp;
	}
	
	public int getDef()
	{
		return _def;
	}
	
	public int getSpDef()
	{
		return _def_sp;
	}
	
	public float getSpd()
	{
		return _spd;
	}
	
	public float getSpSpd()
	{
		return _spd_sp;
	}
	
	public int getAgi()
	{
		return _agi;
	}
	
	public int getSpAgi()
	{
		return _agi_sp;
	}
	
	public int getSpi()
	{
		return _spi;
	}
	
	public int getInt()
	{
		return _int;
	}
	
	public int getPer()
	{
		return _per;
	}
	
	public int getLuck()
	{
		return _luc;
	}
	
	public int getRep()
	{
		return _rep;
	}
	
	public int getHealth()
	{
		return _hp;
	}
	
	public int getMana()
	{
		return _mp;
	}
	
	public int getCharge()
	{
		return _specCharge;
	}
	
	public float getATB()
	{
		return _atb;
	}
	
	public float getATBMax()
	{
		return (100 + ((_lvl * 10) + ((_spd * _lvl) * 5)));
	}
	
	public boolean getReady()
	{
		return _isReady;
	}
	
	public void zeroATB()
	{
		_atb = 0.0f;
		_isReady = false;
	}
	
	public String[] getAbilityHeaders()
	{
		return _moves;
	}
	
	public String[] getExtendedHeaders()
	{
		return _mythicType.getHeaders();
	}
	
	public String[] getRoleHeaders() 
	{
		return _roleHeaders;
	}
	
	public String getTitle()
	{
		String _s = _mythicType.getBase() + " " + _species;
		return _s;
	}
	
	public void act_MoveExt1() {
		_mythicType.act_MoveExt1(this, this);
	}

	public void act_MoveExt2() {
		_mythicType.act_MoveExt2(this, this);
		
	}

	public void act_MoveExt3() {
		_mythicType.act_MoveExt3(this, this);
		
	}

	public void act_MoveExt4() {
		_mythicType.act_MoveExt4(this, this);
		
	}

	public void act_MoveExt5() {
		_mythicType.act_MoveExt5(this, this);
		
	}

	public void act_MoveExt6() {
		_mythicType.act_MoveExt6(this, this);
		
	}

	public void act_MoveExt7() {
		_mythicType.act_MoveExt7(this, this);
		
	}

	public void act_MoveExt8() {
		_mythicType.act_MoveExt8(this, this);
		
	}
}
