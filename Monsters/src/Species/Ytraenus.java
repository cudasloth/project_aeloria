package Species;

import ElementalType.Unknown;

public class Ytraenus extends Dragon implements Unknown
{
	public Ytraenus()
	{
		_elementalType = _baseType;
		_mythicType = Engine.DragonFarm.getInstance().getMythic();
		_species = "Ytraenus";
	}

	@Override
	public void AncientVoices(Dragon _target, Dragon _source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void EchoesPast(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Apocalypse(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void AetherStorm(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void PlaceOfStillness(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void BlackThunder(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Arcania(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void EternalSoul(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Obliteration(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void CoreSurge(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MatterAlignment(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void PlanetaryAlignment(Dragon _t, Dragon _s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move5() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move6() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move7() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move8() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move9() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move10() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move11() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_Move12() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt5() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt6() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt7() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveExt8() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_MoveRoleSelect(int _i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void act_DoAutomated() {
		// TODO Auto-generated method stub
		
	}

	

}
