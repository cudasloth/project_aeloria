package Species;

import ElementalType.Water;
import Engine.EngineBase;
import Roles.DPS;

public class Lucida extends Dragon implements Water, DPS
{
	public Lucida()
	{
		_elementalType = _baseType;
		_role = _baseRole;
		_moves = _abilityHeaders;
		GenerateRoleHeaders();
		
		_mythicType = Engine.DragonFarm.getInstance().getMythic();
		_species = "Lucida";
	}
	
	@Override
	public void act_Move1() {
		AbilityLibrary.getInstance().Squirt(this, this);
		
	}

	@Override
	public void act_Move2() {
		AbilityLibrary.getInstance().Splash(this, this);
		
	}

	@Override
	public void act_Move3() {
		AbilityLibrary.getInstance().TidalSurge(this, this);
		
	}

	@Override
	public void act_Move4() {
		AbilityLibrary.getInstance().Foam(this, this);
		
	}

	@Override
	public void act_Move5() {
		AbilityLibrary.getInstance().Bubblejet(this, this);
		
	}

	@Override
	public void act_Move6() {
		AbilityLibrary.getInstance().Float(this, this);
		
	}

	@Override
	public void act_Move7() {
		AbilityLibrary.getInstance().Sink(this, this);
		
	}

	@Override
	public void act_Move8() {
		AbilityLibrary.getInstance().DepthCharge(this, this);
		
	}

	@Override
	public void act_Move9() {
		AbilityLibrary.getInstance().Soak(this, this);
		
	}

	@Override
	public void act_Move10() {
		AbilityLibrary.getInstance().Waterjet(this, this);
		
	}

	@Override
	public void act_Move11() {
		AbilityLibrary.getInstance().LeviathansSong(this, this);
		
	}

	@Override
	public void act_Move12() {
		AbilityLibrary.getInstance().Tsunami(this, this);
		
	}

	//==================================================================================================================================================================================================
	@Override
	public void act_MoveRole1() {
		AbilityLibrary.getInstance().BladeStorm(this, this);
	}

	@Override
	public void act_MoveRole2() {
		AbilityLibrary.getInstance().Quicken(this, this);		
	}

	@Override
	public void act_MoveRole3() {
		AbilityLibrary.getInstance().Lascerate(this, this);		
	}

	@Override
	public void act_MoveRole4() {
		AbilityLibrary.getInstance().Scar(this, this);		
	}

	@Override
	public void act_MoveRole5() {
		AbilityLibrary.getInstance().Leech(this, this);		
	}

	@Override
	public void act_MoveRole6() {
		AbilityLibrary.getInstance().FastFeet(this, this);		
	}

	@Override
	public void act_MoveRole7() {
		AbilityLibrary.getInstance().Streamline(this, this);		
	}

	@Override
	public void act_MoveRole8() {
		AbilityLibrary.getInstance().Storm(this, this);		
	}

	@Override
	public void act_MoveRole9() {
		AbilityLibrary.getInstance().Sap(this, this);		
	}

	@Override
	public void act_MoveRole10() {
		AbilityLibrary.getInstance().PlateFlurry(this, this);		
	}

	@Override
	public void act_MoveRole11() {
		AbilityLibrary.getInstance().Moonshot(this, this);		
	}

	@Override
	public void act_MoveRole12() {
		AbilityLibrary.getInstance().Vine(this, this);		
	}

	@Override
	public void act_MoveRole13() {
		AbilityLibrary.getInstance().Taint(this, this);		
	}

	@Override
	public void act_MoveRole14() {
		AbilityLibrary.getInstance().Slow(this, this);		
	}

	@Override
	public void act_MoveRole15() {
		AbilityLibrary.getInstance().Dart(this, this);		
	}

	@Override
	public void act_MoveRole16() {
		AbilityLibrary.getInstance().Lash(this, this);		
	}

	@Override
	public void act_MoveRole17() {
		AbilityLibrary.getInstance().Enrage(this, this);		
	}

	@Override
	public void act_MoveRole18() {
		AbilityLibrary.getInstance().Blur(this, this);		
	}

	@Override
	public void act_MoveRole19() {
		AbilityLibrary.getInstance().Mirage(this, this);		
	}

	@Override
	public void act_MoveRole20() {
		AbilityLibrary.getInstance().Blind(this, this);		
	}

	@Override
	public void act_MoveRole21() {
		AbilityLibrary.getInstance().Dust(this, this);		
	}

	@Override
	public void act_MoveRole22() {
		AbilityLibrary.getInstance().LethalPresence(this, this);		
	}

	@Override
	public void act_MoveRole23() {
		AbilityLibrary.getInstance().Throw(this, this);		
	}

	@Override
	public void act_MoveRole24() {
		AbilityLibrary.getInstance().ElementalCall(this, this);		
	}

	@Override
	public void act_MoveRole25() {
		AbilityLibrary.getInstance().Parch(this, this);		
	}

	@Override
	public void act_MoveRole26() {
		AbilityLibrary.getInstance().Hellstorm(this, this);		
	}

	@Override
	public void act_MoveRole27() {
		AbilityLibrary.getInstance().Swipe(this, this);		
	}

	@Override
	public void act_MoveRole28() {
		AbilityLibrary.getInstance().SpiritAura(this, this);		
	}

	@Override
	public void act_MoveRole29() {
		AbilityLibrary.getInstance().Patience(this, this);		
	}

	@Override
	public void act_MoveRole30() {
		AbilityLibrary.getInstance().Distract(this, this);		
	}

	@Override
	public void act_MoveRole31() {
		AbilityLibrary.getInstance().Sacrifice(this, this);		
	}

	@Override
	public void act_MoveRole32() {
		AbilityLibrary.getInstance().Itreia(this, this);		
	}

	@Override
	public void act_MoveRoleSelect(int _i) {
		if(_roleIDs[_i] == 0)
			this.act_MoveRole1();
		if(_roleIDs[_i] == 1)
			this.act_MoveRole2();
		if(_roleIDs[_i] == 2)
			this.act_MoveRole3();
		if(_roleIDs[_i] == 3)
			this.act_MoveRole4();
		if(_roleIDs[_i] == 4)
			this.act_MoveRole5();
		if(_roleIDs[_i] == 5)
			this.act_MoveRole6();
		if(_roleIDs[_i] == 6)
			this.act_MoveRole7();
		if(_roleIDs[_i] == 7)
			this.act_MoveRole8();
		if(_roleIDs[_i] == 8)
			this.act_MoveRole9();
		if(_roleIDs[_i] == 9)
			this.act_MoveRole10();
		if(_roleIDs[_i] == 10)
			this.act_MoveRole11();
		if(_roleIDs[_i] == 11)
			this.act_MoveRole12();
		if(_roleIDs[_i] == 12)
			this.act_MoveRole13();
		if(_roleIDs[_i] == 13)
			this.act_MoveRole14();
		if(_roleIDs[_i] == 14)
			this.act_MoveRole15();
		if(_roleIDs[_i] == 15)
			this.act_MoveRole16();
		if(_roleIDs[_i] == 16)
			this.act_MoveRole17();
		if(_roleIDs[_i] == 17)
			this.act_MoveRole18();
		if(_roleIDs[_i] == 18)
			this.act_MoveRole19();
		if(_roleIDs[_i] == 19)
			this.act_MoveRole20();
		if(_roleIDs[_i] == 20)
			this.act_MoveRole21();
		if(_roleIDs[_i] == 21)
			this.act_MoveRole22();
		if(_roleIDs[_i] == 22)
			this.act_MoveRole23();
		if(_roleIDs[_i] == 23)
			this.act_MoveRole24();
		if(_roleIDs[_i] == 24)
			this.act_MoveRole25();
		if(_roleIDs[_i] == 25)
			this.act_MoveRole26();
		if(_roleIDs[_i] == 26)
			this.act_MoveRole27();
		if(_roleIDs[_i] == 27)
			this.act_MoveRole28();
		if(_roleIDs[_i] == 28)
			this.act_MoveRole29();
		if(_roleIDs[_i] == 29)
			this.act_MoveRole30();
		if(_roleIDs[_i] == 30)
			this.act_MoveRole31();
		if(_roleIDs[_i] == 31)
			this.act_MoveRole32();
		
	}

	@Override
	public void GenerateRoleHeaders() 
	{
		for(int i = 0; i < 8; ++i)
		{
			_roleIDs[i] = Engine.EngineBase.getInstance().getRandomInt(32);
			_roleHeaders[i] = _baseRoleHeaders[_roleIDs[i]];
		}
	}

	@Override
	public void act_DoAutomated() 
	{
		int _r = EngineBase.getInstance().getRandomInt(28);
		
		if(_r == 0)
			this.act_Move1();
		if(_r == 1)
			this.act_Move2();
		if(_r == 2)
			this.act_Move3();
		if(_r == 3)
			this.act_Move4();
		if(_r == 4)
			this.act_Move5();
		if(_r == 5)
			this.act_Move6();
		if(_r == 6)
			this.act_Move7();
		if(_r == 7)
			this.act_Move8();
		if(_r == 8)
			this.act_Move9();
		if(_r == 9)
			this.act_Move10();
		if(_r == 10)
			this.act_Move11();
		if(_r == 11)
			this.act_Move12();
		if(_r == 12)
			this.act_MoveExt1();
		if(_r == 13)
			this.act_MoveExt2();
		if(_r == 14)
			this.act_MoveExt3();
		if(_r == 15)
			this.act_MoveExt4();
		if(_r == 16)
			this.act_MoveExt5();
		if(_r == 17)
			this.act_MoveExt6();
		if(_r == 18)
			this.act_MoveExt7();
		if(_r == 19)
			this.act_MoveExt8();
		if(_r == 20)
			this.act_MoveRoleSelect(0);
		if(_r == 21)
			this.act_MoveRoleSelect(1);
		if(_r == 22)
			this.act_MoveRoleSelect(2);
		if(_r == 23)
			this.act_MoveRoleSelect(3);
		if(_r == 24)
			this.act_MoveRoleSelect(4);
		if(_r == 25)
			this.act_MoveRoleSelect(5);
		if(_r == 26)
			this.act_MoveRoleSelect(6);
		if(_r == 27)
			this.act_MoveRoleSelect(7);
	}
}
