package Species;

public class AbilityLibrary 
{
	private static AbilityLibrary thisInstance;
	
	private AbilityLibrary()
	{
		
	}
	
	public static AbilityLibrary getInstance()
	{
		if(thisInstance == null)
		{
			thisInstance = new AbilityLibrary();
		}
		return thisInstance;
	}
	
	//Fire
	public void FireBall(Dragon _target, Dragon _source)
	{
		System.out.println("Fireball called");
	}
	
	public void Burn(Dragon _t, Dragon _s)
	{
		System.out.println("Burn called");
	}
	
	public void EngulfingFlames(Dragon _t, Dragon _s)
	{
		System.out.println("EngulfingFlames called");
	}
	
	public void Volcano(Dragon _t, Dragon _s)
	{
		System.out.println("Volcano called");
	}
	
	public void BurningClaw(Dragon _t, Dragon _s)
	{
		System.out.println("BurningClaw called");
	}
	
	public void TemplesCalling(Dragon _t, Dragon _s)
	{
		System.out.println("TemplesCalling called");
	}
	
	public void PurityFlame(Dragon _t, Dragon _s)
	{
		System.out.println("PurityFlame called");
	}
	
	public void Burrow(Dragon _t, Dragon _s)
	{
		System.out.println("Burrow called");
	}
	
	public void MagmaRelease(Dragon _t, Dragon _s)
	{
		System.out.println("MagmaRelease called");
	}
	
	public void GodsHand(Dragon _t, Dragon _s)
	{
		System.out.println("GodsHand called");
	}
	
	public void ANewFire(Dragon _t, Dragon _s)
	{
		System.out.println("ANewFire called");
	}
	
	public void Firestorm(Dragon _t, Dragon _s)
	{
		System.out.println("Firestorm called");
	}
	
	//Ice
	public void Blizzard(Dragon _target, Dragon _source) 
	{
		System.out.println("Blizzard called");		
	}
	
	public void Freeze(Dragon _t, Dragon _s) 
	{
		System.out.println("Freeze called");
	}
	
	public void Snowfall(Dragon _t, Dragon _s) 
	{
		System.out.println("Snowfall called");
	}
	
	public void Icicle(Dragon _t, Dragon _s) 
	{
		System.out.println("Icicle called");
	}
	
	public void ColdBreath(Dragon _t, Dragon _s) 
	{
		System.out.println("Cold Breath called");
	}
	
	public void Snowflake(Dragon _t, Dragon _s)
	{
		System.out.println("Snowflake called");
	}
	
	public void IceWall(Dragon _t, Dragon _s) 
	{
		System.out.println("IceWall called");
	}
	
	public void ShivasSong(Dragon _t, Dragon _s)
	{
		System.out.println("ShivasSong called");
	}
	
	public void Whiteout(Dragon _t, Dragon _s) 
	{
		System.out.println("Whiteout called");
	}
	
	//Aether
	public void ArcaneChannel(Dragon _t, Dragon _s)
	{		
		System.out.println("ArcaneChannel Called");	
	}
	
	public void Prophecy(Dragon _t, Dragon _s)
	{		
		System.out.println("Prophecy Called");	
	}
	
	public void MythicShift(Dragon _t, Dragon _s)
	{		
		System.out.println("MythicShift Called");	
	}
	
	public void MatterBeam(Dragon _t, Dragon _s)
	{		
		System.out.println("MatterBeam Called");	
	}
	
	public void Apocalypse(Dragon _t, Dragon _s)
	{		
		System.out.println("Apocalypse Called");	
	}
	
	public void Foresee(Dragon _t, Dragon _s)
	{		
		System.out.println("Foresee Called");	
	}
	
	public void CoreEssence(Dragon _t, Dragon _s)
	{		
		System.out.println("CoreEssence Called");	
	}
	
	public void AetherStorm(Dragon _t, Dragon _s)
	{		
		System.out.println("AetherStorm Called");	
	}
	
	//Chrono
	public void Rewind(Dragon _t, Dragon _s) 
	{
	}
	
	public void QuantumCrack(Dragon _t, Dragon _s)
	{
	}
	
	public void ChronometricImpulse(Dragon _t, Dragon _s)
	{
	}
	
	public void CallOfThartan(Dragon _t, Dragon _s) 
	{
	}
	
	public void SpeedBoost(Dragon _t, Dragon _s)
	{
	}
	
	public void CrystallineManipulation(Dragon _t, Dragon _s)
	{
	}
	
	public void Aftermath(Dragon _t, Dragon _s) 
	{
	}
	
	public void ChronoBlade(Dragon _t, Dragon _s) 
	{
	}
	
	public void FarCry(Dragon _t, Dragon _s)
	{
	}
	
	public void Reminiscence(Dragon _t, Dragon _s) 
	{
	}
	
	public void PlaceOfStillness(Dragon _t, Dragon _s) 
	{
	}
	
	public void Stop(Dragon _t, Dragon _s) 
	{
	}
	
	//Crystal
	public void Shatter(Dragon _t, Dragon _s) 
	{
	}
	
	public void Solidify(Dragon _t, Dragon _s) 
	{
	}
	
	public void Atune(Dragon _t, Dragon _s) 
	{
	}
	
	public void LightArray(Dragon _t, Dragon _s) 
	{
	}
	
	public void CallOfTheDivine(Dragon _t, Dragon _s) 
	{
	}
	
	public void UniversalToning(Dragon _t, Dragon _s) 
	{
	}
	
	public void HealingEssence(Dragon _t, Dragon _s) 
	{
	}
	
	public void CrystallineImpurity(Dragon _t, Dragon _s) 
	{
	}
	
	public void Resonance(Dragon _t, Dragon _s) 
	{
	}
	
	public void UniversalVibrations(Dragon _t, Dragon _s)
	{
	}
	
	public void Shatterbreak(Dragon _t, Dragon _s) 
	{
	}
	
	//Dark
	public void Claw(Dragon _t, Dragon _s) 
	{
	}
	
	public void ShadowBall(Dragon _t, Dragon _s)
	{
	}
	
	public void DarkeningCurse(Dragon _t, Dragon _s)
	{
	}
	
	public void Black(Dragon _t, Dragon _s) 
	{
	}
	
	public void ThunderingDarkness(Dragon _t, Dragon _s)
	{
	}
	
	public void NightCall(Dragon _t, Dragon _s) 
	{
	}
	
	public void Gouge(Dragon _t, Dragon _s) 
	{
	}
	
	public void ImposingPresence(Dragon _t, Dragon _s) 
	{
	}
	
	public void Hide(Dragon _t, Dragon _s) 
	{
	}
	
	public void DecrepidBlast(Dragon _t, Dragon _s) 
	{
	}
	
	public void BlackThunder(Dragon _t, Dragon _s)
	{
	}
	
	//Devora
	public void TribalCall(Dragon _t, Dragon _s)
	{
	}
	
	public void ArmorBurst(Dragon _t, Dragon _s) 
	{
	}
	
	public void EarthShatter(Dragon _t, Dragon _s) 
	{
	}
	
	public void Sandstorm(Dragon _t, Dragon _s) 
	{
	}
	
	public void WarringCall(Dragon _t, Dragon _s) 
	{
	}
	
	public void Thrash(Dragon _t, Dragon _s) 
	{
	}
	
	public void Embolden(Dragon _t, Dragon _s) 
	{
	}
	
	public void AmiriaDevora(Dragon _t, Dragon _s) 
	{
	}
	
	public void SpiritualHaunting(Dragon _t, Dragon _s) 
	{
	}
	
	public void Eviscerate(Dragon _t, Dragon _s) 
	{
	}
	
	public void DevoricSummoning(Dragon _t, Dragon _s) 
	{
	}
	
	//Drake
	public void Fly(Dragon _t, Dragon _s) 
	{
	}
	
	public void Divebomb(Dragon _t, Dragon _s) 
	{
	}
	
	public void Scout(Dragon _t, Dragon _s)
	{
	}
	
	public void DropClaw(Dragon _t, Dragon _s) 
	{
	}
	
	public void Bombard(Dragon _t, Dragon _s) 
	{
	}
	
	public void Target(Dragon _t, Dragon _s) 
	{
	}
	
	public void Identify(Dragon _t, Dragon _s) 
	{
	}
	
	public void Bite(Dragon _t, Dragon _s) 
	{
	}
	
	public void SkyCall(Dragon _t, Dragon _s) 
	{
	}
	
	public void GatherSenses(Dragon _t, Dragon _s)
	{
	}
	
	public void SunBlock(Dragon _t, Dragon _s) 
	{
	}
	
	public void TacticalRush(Dragon _t, Dragon _s) 
	{
	}
	
	//Eternal
	public void EtherealPresence(Dragon _t, Dragon _s)
	{
	}
	
	public void Heavy(Dragon _t, Dragon _s) 
	{
	}
	
	public void ImobilePresence(Dragon _t, Dragon _s)
	{
	}
	
	public void SoulShatter(Dragon _t, Dragon _s) 
	{
	}
	
	public void Surge(Dragon _t, Dragon _s)
	{
	}
	
	public void Arcania(Dragon _t, Dragon _s)
	{
		System.out.println("Arcania Called");
	}
	
	public void EternalSoul(Dragon _t, Dragon _s)
	{
	}
	
	public void Obliteration(Dragon _t, Dragon _s) 
	{
	}
	
	//Graviton
	public void Gravity(Dragon _t, Dragon _s) 
	{
	}
	
	public void GravitonBeam(Dragon _t, Dragon _s)
	{
	}
	
	public void Stonewall(Dragon _t, Dragon _s) 
	{
	}
	
	public void ThrowWeight(Dragon _t, Dragon _s) 
	{
	}
	
	public void CoreSurge(Dragon _t, Dragon _s) 
	{
	}
	
	public void MatterAlignment(Dragon _t, Dragon _s)
	{
	}
	
	//Holy
	public void HolyClaw(Dragon _t, Dragon _s) 
	{
	}
	
	public void RebornSpirit(Dragon _t, Dragon _s)
	{
	}
	
	public void HolySpear(Dragon _t, Dragon _s) 
	{
	}
	
	public void LightStorm(Dragon _t, Dragon _s) 
	{
		System.out.println("LightStorm Called");
	}
	
	public void Evanescence(Dragon _t, Dragon _s) 
	{
	}
	
	public void PlanetaryAlignment(Dragon _t, Dragon _s)
	{
	}
	
	//Judgement
	public void Hammerfall(Dragon _target, Dragon _source) 
	{
	}
	
	public void ScalesOfEquality(Dragon _t, Dragon _s) 
	{
	}
	
	public void HistoriesWrath(Dragon _t, Dragon _s) 
	{
	}
	
	public void Vengeance(Dragon _t, Dragon _s) 
	{
	}
	
	public void JudgementOfAntares(Dragon _t, Dragon _s) 
	{
	}
	
	public void Breakheart(Dragon _t, Dragon _s) 
	{
	}
	
	public void Balance(Dragon _t, Dragon _s) 
	{
	}
	
	public void ForlornHope(Dragon _t, Dragon _s)
	{
	}
	
	public void CallOfAntares(Dragon _t, Dragon _s)
	{
	}
	
	//Kathuul
	public void AncientVoices(Dragon _target, Dragon _source) 
	{
	}
	
	public void EchoesPast(Dragon _t, Dragon _s)
	{
	}
	
	//Krilteka
	public void EmpiresEcho(Dragon _target, Dragon _source) 
	{
	}
	
	public void SoulsOfTheFallen(Dragon _t, Dragon _s) 
	{
	}
	
	public void ForTheEmpire(Dragon _t, Dragon _s) 
	{
	}
	
	public void CallTheCavalry(Dragon _t, Dragon _s) 
	{
	}
	
	public void Quickening(Dragon _t, Dragon _s) 
	{
	}
	
	public void Organise(Dragon _t, Dragon _s) 
	{
	}
	
	public void SpiritOfTheCapitol(Dragon _t, Dragon _s) 
	{
	}
	
	//Light
	public void Flash(Dragon _target, Dragon _source) 
	{
		System.out.println("Flash Called");
	}
	
	public void FocussedLight(Dragon _t, Dragon _s) 
	{
		System.out.println("FocussedLight Called");
	}
	
	public void BlindingShards(Dragon _t, Dragon _s) 
	{
		System.out.println("BlindingShards Called");
	}
	
	public void Harmony(Dragon _t, Dragon _s) 
	{
		System.out.println("Harmony Called");
	}
	
	public void SoulsCaress(Dragon _t, Dragon _s) 
	{
		System.out.println("SoulsCaress Called");
	}

	public void SunsFlare(Dragon _t, Dragon _s) 
	{
		System.out.println("SunsFlare Called");
	}
	
	public void Shinedown(Dragon _t, Dragon _s) 
	{
		System.out.println("Shinedown Called");
	}
	
	public void PaladinsLight(Dragon _t, Dragon _s)
	{
		System.out.println("PaladinsLight Called");
	}
	
	public void Embrace(Dragon _t, Dragon _s)
	{
		System.out.println("Embrace Called");
	}
	
	public void Sunscar(Dragon _t, Dragon _s)
	{
		System.out.println("Sunscar Called");
	}
	
	//Lunar
	public void LunarFlare(Dragon _target, Dragon _source) 
	{
	}
	
	public void LunarCalling(Dragon _t, Dragon _s)
	{
	}
	
	public void LunarInterference(Dragon _t, Dragon _s) 
	{
	}
	
	public void LunarEclipse(Dragon _t, Dragon _s)
	{
	}
	
	//Lyreal
	
	//Mana
	
	//Normal	
	public void Charge(Dragon _t, Dragon _s) 
	{
	}
	
	public void Wing(Dragon _t, Dragon _s)
	{
	}
	
	public void Glare(Dragon _t, Dragon _s) 
	{
	}
	
	public void Hit(Dragon _t, Dragon _s) 
	{
	}
	
	public void FrighteningGaze(Dragon _t, Dragon _s) 
	{
	}
	
	public void Tear(Dragon _t, Dragon _s)
	{
	}
	
	public void Headbutt(Dragon _t, Dragon _s)
	{
	}
	
	public void FormationCharge(Dragon _t, Dragon _s) 
	{
	}
	
	//Obsidian
	
	//Plant
	
	//Quell
	
	//River
	
	//Silicon
	
	//Snow
	
	//Solar
	
	//Soul
	
	//Stunted
	
	//Terran
	public void Rumble(Dragon _target, Dragon _source) 
	{
		System.out.println("Rumble Called");
	}
	
	public void Crack(Dragon _t, Dragon _s) 
	{
		System.out.println("Crack Called");
	}
	
	public void RockFall(Dragon _t, Dragon _s) 
	{
		System.out.println("RockFall Called");
	}
	
	public void Barricade(Dragon _t, Dragon _s) 
	{
		System.out.println("Barricade Called");
	}
	
	public void EarthSwallow(Dragon _t, Dragon _s) 
	{
		System.out.println("EarthSwallow Called");
	}
	
	public void Earthquake(Dragon _t, Dragon _s) 
	{
		System.out.println("Earthquake Called");
	}
	
	public void RockStorm(Dragon _t, Dragon _s)
	{
		System.out.println("RockStorm Called");
	}
	
	public void Subsidance(Dragon _t, Dragon _s) 
	{
		System.out.println("Subsidance Called");
	}
	
	public void FaultLine(Dragon _t, Dragon _s)
	{
		System.out.println("FaultLine Called");
	}
	
	public void Landmass(Dragon _t, Dragon _s) 
	{
		System.out.println("Landmass Called");
	}
	
	public void PlanetaryBreak(Dragon _t, Dragon _s) 
	{
		System.out.println("PlanetaryBreak Called");
	}
	
	public void EchoesOfTerra(Dragon _t, Dragon _s) 
	{
		System.out.println("EchoesOfTerra Called");
	}
	
	//Toxic
	
	//Ultimus
	
	//Undead
	
	//Unknown
	
	//Valence
	
	//Water
	public void Squirt(Dragon _target, Dragon _source) 
	{
		System.out.println("Squirt Called");
	}
	
	public void Splash(Dragon _t, Dragon _s) 
	{
		System.out.println("Splash Called");
	}
	
	public void TidalSurge(Dragon _t, Dragon _s) 
	{
		System.out.println("TidalSurge Called");
	}
	
	public void Foam(Dragon _t, Dragon _s) 
	{
		System.out.println("Foam Called");
	}
	
	public void Bubblejet(Dragon _t, Dragon _s)
	{
		System.out.println("Bubblejet Called");
	}
	
	public void Float(Dragon _t, Dragon _s)
	{
		System.out.println("Float Called");
	}
	
	public void Sink(Dragon _t, Dragon _s) 
	{
		System.out.println("Sink Called");
	}
	
	public void DepthCharge(Dragon _t, Dragon _s)
	{
		System.out.println("DepthCharge Called");
	}
	
	public void Soak(Dragon _t, Dragon _s)
	{
		System.out.println("Soak Called");
	}
	
	public void Waterjet(Dragon _t, Dragon _s)
	{
		System.out.println("Waterjet Called");
	}
	
	public void LeviathansSong(Dragon _t, Dragon _s)
	{
		System.out.println("LeviathansSong Called");
	}
	
	public void Tsunami(Dragon _t, Dragon _s)
	{
		System.out.println("Tsunami Called");
	}
	
	//Wind
	
	//Wyvern
	
	//Zaal
	
	//==================================================================================================================================================================================================
	
	//Baby
	public void Flail(Dragon _t, Dragon _s) {
		System.out.println("Flail Called");
		
	}

	public void Wiggle(Dragon _t, Dragon _s) {
		System.out.println("Wiggle Called");
		
	}

	public void TailSlap(Dragon _t, Dragon _s) {
		System.out.println("TailSlap Called");
		
	}

	public void Hiss(Dragon _t, Dragon _s) {
		System.out.println("Hiss Called");
		
	}

	public void Spurt(Dragon _t, Dragon _s) {
		System.out.println("Spurt Called");
		
	}

	public void Cover(Dragon _t, Dragon _s) {
		System.out.println("Cover Called");
		
	}

	public void Hover(Dragon _t, Dragon _s) {
		System.out.println("Hover Called");
		
	}

	public void DangerScream(Dragon _t, Dragon _s) {
		System.out.println("DangerScream Called");
		
	}
	
	//Outcast
	public void CursedStrike(Dragon _t, Dragon _s) {
		System.out.println("CursedStrike Called");
		
	}

	public void FallenSoul(Dragon _t, Dragon _s) {
		System.out.println("FallenSoul Called");
		
	}

	public void Tempt(Dragon _t, Dragon _s) {
		System.out.println("Tempt Called");
		
	}

	public void Relive(Dragon _t, Dragon _s) {
		System.out.println("Relive Called");
		
	}

	public void TornSoul(Dragon _t, Dragon _s) {
		System.out.println("TornSoul Called");
		
	}

	public void Atone(Dragon _t, Dragon _s) {
		System.out.println("Atone Called");
		
	}

	public void Curse(Dragon _t, Dragon _s) {
		System.out.println("Curse Called");
		
	}

	public void OutcastsGrief(Dragon _t, Dragon _s) {
		System.out.println("OutcastsGrief Called");
		
	}
	
	//Young
	public void Takedown(Dragon _t, Dragon _s) {
		System.out.println("Takedown Called");
		
	}
	
	public void Glide(Dragon _t, Dragon _s) {
		System.out.println("Glide Called");
		
	}
	
	public void StandTall(Dragon _t, Dragon _s) {
		System.out.println("StandTall Called");
		
	}
	
	//Lesser
	
	//Greater
	
	//==================================================================================================================================================================================================
	//Tank
	
	//Healer
	public void Cure(Dragon _t, Dragon _s) 
	{
		System.out.println("Cure Called");
	}
	
	public void FocusCure(Dragon _t, Dragon _s) 
	{
		System.out.println("FocusCure Called");
	}
	
	public void SpiritCure(Dragon _t, Dragon _s) 
	{
		System.out.println("SpiritCure Called");
	}
	
	public void Cleanse(Dragon _t, Dragon _s) 
	{
		System.out.println("Cleanse Called");
	}
	
	public void Life(Dragon _t, Dragon _s) 
	{
		System.out.println("Life Called");
	}
	public void Partra(Dragon _t, Dragon _s) 
	{
		System.out.println("Partra Called");
	}
	
	public void Lifa(Dragon _t, Dragon _s) 
	{
		System.out.println("Lifa Called");
	}
	
	public void Purify(Dragon _t, Dragon _s) 
	{
		System.out.println("Purify Called");
	}
	
	public void Focus(Dragon _t, Dragon _s) 
	{
		System.out.println("Focus Called");
	}
	
	public void Grace(Dragon _t, Dragon _s) 
	{
		System.out.println("Grace Called");
	}
	
	public void ProtectionAura(Dragon _t, Dragon _s) 
	{
		System.out.println("ProtectionAura Called");
	}
	
	public void Haste(Dragon _t, Dragon _s)
	{
		System.out.println("Haste Called");
	}
	
	public void Fortify(Dragon _t, Dragon _s) 
	{
		System.out.println("Fortify Called");
	}
	
	public void Clarity(Dragon _t, Dragon _s) 
	{
		System.out.println("Clarity Called");
	}
	
	public void DivineBlessing(Dragon _t, Dragon _s) 
	{
		System.out.println("DivineBlessing Called");
	}
	
	public void Bless(Dragon _t, Dragon _s) 
	{
		System.out.println("Bless Called");
	}
	
	public void PriestessBell(Dragon _t, Dragon _s) 
	{
		System.out.println("PriestessBell Called");
	}
	
	public void CallOfYnatra(Dragon _t, Dragon _s)
	{
		System.out.println("CallOfYnatra Called");
	}
	
	public void SoulsVoice(Dragon _t, Dragon _s) 
	{
		System.out.println("SoulsVoice Called");
	}
	
	public void Eclara(Dragon _t, Dragon _s) 
	{
		System.out.println("Eclara Called");
	}
	
	public void GoddessTear(Dragon _t, Dragon _s) 
	{
		System.out.println("GoddessTear Called");
	}
	
	public void ElissasSong(Dragon _t, Dragon _s)
	{
		System.out.println("ElissasSong Called");
	}
	
	public void RallyingCry(Dragon _t, Dragon _s) 
	{
		System.out.println("RallyingCry Called");
	}
	
	public void Cleric(Dragon _t, Dragon _s)
	{
		System.out.println("Cleric Called");
	}
	
	public void BardsSong(Dragon _t, Dragon _s) 
	{
		System.out.println("BardsSong Called");
	}
	
	public void Fortune(Dragon _t, Dragon _s) 
	{
		System.out.println("Fortune Called");
	}
	
	public void Natura(Dragon _t, Dragon _s) 
	{
		System.out.println("Natura Called");
	}
	
	public void RageOfGods(Dragon _t, Dragon _s) 
	{
		System.out.println("RageOfGods Called");
	}
	
	public void Holy(Dragon _t, Dragon _s) 
	{
		System.out.println("Holy Called");
	}
	
	public void FocusHoly(Dragon _t, Dragon _s) 
	{
		System.out.println("FocusHoly Called");
	}
	
	public void ShatteredEssence(Dragon _t, Dragon _s)
	{
		System.out.println("ShatteredEssence Called");
	}
	
	public void SoulsLament(Dragon _t, Dragon _s)
	{
		System.out.println("SoulsLament Called");
	}
	
	//DPS
	public void BladeStorm(Dragon _t, Dragon _s)
	{
		System.out.println("BladeStorm Called");
	}
	
	public void Quicken(Dragon _t, Dragon _s) 
	{
		System.out.println("Quicken Called");
	}
	
	public void Lascerate(Dragon _t, Dragon _s) 
	{
		System.out.println("Lascerate Called");
	}
	
	public void Scar(Dragon _t, Dragon _s) 
	{
		System.out.println("Scar Called");
	}
	
	public void Leech(Dragon _t, Dragon _s) 
	{
		System.out.println("Leech Called");
	}
	
	public void FastFeet(Dragon _t, Dragon _s) 
	{
		System.out.println("FastFeet Called");
	}
	
	public void Streamline(Dragon _t, Dragon _s)
	{
		System.out.println("Streamline Called");
	}
	
	public void Storm(Dragon _t, Dragon _s) 
	{
		System.out.println("Storm Called");
	}
	
	public void Sap(Dragon _t, Dragon _s) 
	{
		System.out.println("Sap Called");
	}
	
	public void PlateFlurry(Dragon _t, Dragon _s) 
	{
		System.out.println("PlateFlurry Called");
	}
	
	public void Moonshot(Dragon _t, Dragon _s)
	{
		System.out.println("Moonshot Called");
	}
	
	public void Vine(Dragon _t, Dragon _s) 
	{
		System.out.println("Vine Called");
	}
	
	public void Taint(Dragon _t, Dragon _s)
	{
		System.out.println("Taint Called");
	}
	
	public void Slow(Dragon _t, Dragon _s) 
	{
		System.out.println("Slow Called");
	}
	
	public void Dart(Dragon _t, Dragon _s)
	{
		System.out.println("Dart Called");
	}
	
	public void Lash(Dragon _t, Dragon _s) 
	{
		System.out.println("Lash Called");
	}
	
	public void Enrage(Dragon _t, Dragon _s) 
	{
		System.out.println("Enrage Called");
	}
	
	public void Blur(Dragon _t, Dragon _s)
	{
		System.out.println("Blur Called");
	}
	
	public void Mirage(Dragon _t, Dragon _s)
	{
		System.out.println("Mirage Called");
	}
	
	public void Blind(Dragon _t, Dragon _s)
	{
		System.out.println("Blind Called");
	}
	
	public void Dust(Dragon _t, Dragon _s) 
	{
		System.out.println("Dust Called");
	}
	
	public void LethalPresence(Dragon _t, Dragon _s)
	{
		System.out.println("LethalPresence Called");
	}
	
	public void Throw(Dragon _t, Dragon _s)
	{
		System.out.println("Throw Called");
	}
	
	public void ElementalCall(Dragon _t, Dragon _s) 
	{
		System.out.println("ElementalCall Called");
	}
	
	public void Parch(Dragon _t, Dragon _s)
	{
		System.out.println("Parch Called");
	}
	
	public void Hellstorm(Dragon _t, Dragon _s) 
	{
		System.out.println("Hellstorm Called");
	}
	
	public void Swipe(Dragon _t, Dragon _s) 
	{
		System.out.println("Swipe Called");
	}
	
	public void SpiritAura(Dragon _t, Dragon _s)
	{
		System.out.println("SpiritAura Called");
	}
	
	public void Patience(Dragon _t, Dragon _s) 
	{
		System.out.println("Patience Called");
	}
	
	public void Distract(Dragon _t, Dragon _s)
	{
		System.out.println("Distract Called");
	}
	
	public void Sacrifice(Dragon _t, Dragon _s) 
	{
		System.out.println("Sacrifice Called");
	}
	
	public void Itreia(Dragon _t, Dragon _s) 
	{
		System.out.println("Itreia Called");
	}
}

