package MythicTypes;

import Species.AbilityLibrary;
import Species.Dragon;

public class BabyDragon extends Mythic 
{
	private String[] _baseHeaders = {"Flail", "Wiggle", "Tail Slap", "Hiss", "Spurt", "Cover", "Hover", "Danger Scream"};
	
	public BabyDragon()
	{
		_mythic = "Baby";
		_mythicHeaders = _baseHeaders;
	}

	@Override
	public void act_MoveExt1(Dragon _target, Dragon _source) {
		AbilityLibrary.getInstance().Flail(_target, _source);
		
	}

	@Override
	public void act_MoveExt2(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Wiggle(_t, _s);
		
	}

	@Override
	public void act_MoveExt3(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().TailSlap(_t, _s);
		
	}

	@Override
	public void act_MoveExt4(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Hiss(_t, _s);
		
	}

	@Override
	public void act_MoveExt5(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Spurt(_t, _s);
		
	}

	@Override
	public void act_MoveExt6(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Cover(_t, _s);
		
	}

	@Override
	public void act_MoveExt7(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Hover(_t, _s);
		
	}

	@Override
	public void act_MoveExt8(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().DangerScream(_t, _s);
		
	}
}
