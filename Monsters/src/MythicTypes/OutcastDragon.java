package MythicTypes;

import Species.AbilityLibrary;
import Species.Dragon;

public class OutcastDragon extends Mythic 
{
	private String[] _baseHeaders = {"Cursed Strike", "Fallen Soul", "Tempt", "Relive", "Torn Soul", "Atone", "Curse", "Outcasts Grief"};
	
	public OutcastDragon()
	{
		_mythic = "Outcast";
		_mythicHeaders = _baseHeaders;
	}
	
	@Override
	public void act_MoveExt1(Dragon _target, Dragon _source) {
		AbilityLibrary.getInstance().CursedStrike(_target, _source);
		
	}

	@Override
	public void act_MoveExt2(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().FallenSoul(_t, _s);
		
	}

	@Override
	public void act_MoveExt3(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Tempt(_t, _s);
		
	}

	@Override
	public void act_MoveExt4(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Relive(_t, _s);
		
	}

	@Override
	public void act_MoveExt5(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().TornSoul(_t, _s);
		
	}

	@Override
	public void act_MoveExt6(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Atone(_t, _s);
		
	}

	@Override
	public void act_MoveExt7(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().Curse(_t, _s);
		
	}

	@Override
	public void act_MoveExt8(Dragon _t, Dragon _s) {
		AbilityLibrary.getInstance().OutcastsGrief(_t, _s);
		
	}
}
