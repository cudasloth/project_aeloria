package MythicTypes;

import Engine.MythicEntity;

public abstract class Mythic implements MythicEntity
{
	protected String _mythic;
	protected String[] _mythicHeaders;
	
	protected int _bonusAtk 		= 0;
	protected int _bonusAtk_sp 		= 0;
	protected int _bonusDef 		= 0;
	protected int _bonusDef_sp 		= 0;
	protected int _bonusSpd 		= 0;
	protected int _bonusSpd_sp		= 0;
	protected int _bonusAgi 		= 0;
	protected int _bonusAgi_sp		= 0;
	protected int _bonusSpi 		= 0;
	protected int _bonusInt 		= 0;
	protected int _bonusPer			= 0;
	protected int _bonusLuc			= 0;
	protected int _bonusRep 		= 0;
	
	public int getBonusAtk() 		{ return _bonusAtk; 		}
	public int getBonusAtkSp() 		{ return _bonusAtk_sp; 		}
	public int getBonusDef() 		{ return _bonusDef; 		}
	public int getBonusDefSp()		{ return _bonusDef_sp; 		}
	public int getBonusSpd()		{ return _bonusSpd;			}
	public int getBonusSpdSp()		{ return _bonusSpd_sp;		}
	public int getBonusAgi()		{ return _bonusAgi;			}
	public int getBonusAgiSp()		{ return _bonusAgi_sp;		}
	public int getBonusSpi()		{ return _bonusSpi; 		}
	public int getBonusInt()		{ return _bonusInt; 		}
	public int getBonusPer()		{ return _bonusPer; 		}
	public int getBonusLuc()		{ return _bonusLuc; 		}
	public int getBonusRep()		{ return _bonusRep; 		}
	
	public String getBase()
	{
		return _mythic;
	}
	
	public String[] getHeaders()
	{
		return _mythicHeaders;
	}
}
