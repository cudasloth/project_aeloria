package Roles;

public interface Healer 
{
	String _baseRole = "Healer";
	String[] _baseRoleHeaders = {"Cure", "Focus Cure", "Spirit Cure", "Cleanse", "Life", "Partra", "Lifa", "Purify", "Focus", "Grace", "Protection Aura", "Haste", "Fortify", "Clarity", "Divine Blessing", "Bless", "Priestess Bell", "Call of Ynatra", "Souls Voice", "Eclara", "Goddess Tear", "Elissa's Song", "Rallying Cry", "Cleric", "Bards Song", "Fortune", "Natura", "Rage of Gods", "Holy", "Focus Holy", "Shattered Essence", "Souls Lament"};
	
	public void act_MoveRole1();
	public void act_MoveRole2();
	public void act_MoveRole3();
	public void act_MoveRole4();
	public void act_MoveRole5();
	public void act_MoveRole6();
	public void act_MoveRole7();
	public void act_MoveRole8();
	public void act_MoveRole9();
	public void act_MoveRole10();
	public void act_MoveRole11();
	public void act_MoveRole12();
	public void act_MoveRole13();
	public void act_MoveRole14();
	public void act_MoveRole15();
	public void act_MoveRole16();
	public void act_MoveRole17();
	public void act_MoveRole18();
	public void act_MoveRole19();
	public void act_MoveRole20();
	public void act_MoveRole21();
	public void act_MoveRole22();
	public void act_MoveRole23();
	public void act_MoveRole24();
	public void act_MoveRole25();
	public void act_MoveRole26();
	public void act_MoveRole27();
	public void act_MoveRole28();
	public void act_MoveRole29();
	public void act_MoveRole30();
	public void act_MoveRole31();
	public void act_MoveRole32();

	public void GenerateRoleHeaders();
}