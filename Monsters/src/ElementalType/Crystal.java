package ElementalType;

import Species.Dragon;

public interface Crystal 
{
	String _baseType = "Crystal";

	public void Shatter(Dragon _t, Dragon _s);
	public void Crack(Dragon _t, Dragon _s);
	public void Solidify(Dragon _t, Dragon _s);
	public void Atune(Dragon _t, Dragon _s);
	public void LightArray(Dragon _t, Dragon _s);
	public void CallOfTheDivine(Dragon _t, Dragon _s);
	public void UniversalToning(Dragon _t, Dragon _s);
	public void HealingEssence(Dragon _t, Dragon _s);
	public void CrystallineImpurity(Dragon _t, Dragon _s);
	public void Resonance(Dragon _t, Dragon _s);
	public void UniversalVibrations(Dragon _t, Dragon _s);
	public void Shatterbreak(Dragon _t, Dragon _s);
}
