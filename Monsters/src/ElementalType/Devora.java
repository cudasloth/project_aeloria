package ElementalType;

import Species.Dragon;

public interface Devora //Tribal, think Jahara FF12
{
	String _baseType = "Devora";

	public void ArcaneChannel(Dragon _t, Dragon _s);
	public void TribalCall(Dragon _t, Dragon _s);
	public void ArmorBurst(Dragon _t, Dragon _s);
	public void EarthShatter(Dragon _t, Dragon _s);
	public void Sandstorm(Dragon _t, Dragon _s);
	public void WarringCall(Dragon _t, Dragon _s);
	public void Thrash(Dragon _t, Dragon _s);
	public void Embolden(Dragon _t, Dragon _s);
	public void AmiriaDevora(Dragon _t, Dragon _s);
	public void SpiritualHaunting(Dragon _t, Dragon _s);
	public void Eviscerate(Dragon _t, Dragon _s);
	public void DevoricSummoning(Dragon _t, Dragon _s);
}
