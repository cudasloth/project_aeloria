package ElementalType;

import Species.Dragon;

public interface Aether
{
	String _baseType = "Aether";
	
	public void ArcaneChannel(Dragon _t, Dragon _s);
	public void Prophecy(Dragon _t, Dragon _s);
	public void MythicShift(Dragon _t, Dragon _s);
	public void MatterBeam(Dragon _t, Dragon _s);
	public void Focus(Dragon _t, Dragon _s);
	public void Apocalypse(Dragon _t, Dragon _s);
	public void Cleanse(Dragon _t, Dragon _s);
	public void Life(Dragon _t, Dragon _s);
	public void Cure(Dragon _t, Dragon _s);
	public void Foresee(Dragon _t, Dragon _s);
	public void CoreEssence(Dragon _t, Dragon _s);
	public void AetherStorm(Dragon _t, Dragon _s);
}
