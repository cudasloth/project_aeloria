package ElementalType;

import Species.Dragon;

public interface Judgement 
{
	String _baseType = "Judgement";

	public void Hammerfall(Dragon _target, Dragon _source);
	public void ScalesOfEquality(Dragon _t, Dragon _s);
	public void HistoriesWrath(Dragon _t, Dragon _s);
	public void Foresee(Dragon _t, Dragon _s);
	public void Vengeance(Dragon _t, Dragon _s);
	public void JudgementOfAntares(Dragon _t, Dragon _s);
	public void Breakheart(Dragon _t, Dragon _s);
	public void Balance(Dragon _t, Dragon _s);
	public void ImposingPresence(Dragon _t, Dragon _s);
	public void ForlornHope(Dragon _t, Dragon _s);
	public void CallOfAntares(Dragon _t, Dragon _s);
	public void Evanescence(Dragon _t, Dragon _s);
}
