package ElementalType;

import Species.Dragon;

public interface Eternal
{
	String _baseType = "Eternal";
	
	public void EtherealPresence(Dragon _t, Dragon _s);
	public void Heavy(Dragon _t, Dragon _s);
	public void ImobilePresence(Dragon _t, Dragon _s);
	public void SoulShatter(Dragon _t, Dragon _s);
	public void Surge(Dragon _t, Dragon _s);
	public void Focus(Dragon _t, Dragon _s);
	public void Cleanse(Dragon _t, Dragon _s);
	public void Eviscerate(Dragon _t, Dragon _s);
	public void Cure(Dragon _t, Dragon _s);
	public void Arcania(Dragon _t, Dragon _s);
	public void EternalSoul(Dragon _t, Dragon _s);
	public void Obliteration(Dragon _t, Dragon _s);
}
