package ElementalType;

//Unfinal
public interface Terran
{
	String _baseType = "Terran";
	String[] _abilityHeaders = {"Rumble", "Crack", "Rock Fall", "Barricade", "Earth Swallow", "Earthquake", "Rock Storm", "Subsidance", "Fault Line", "Landmass", "Planetary Break", "Echoes of Terra"};
}
