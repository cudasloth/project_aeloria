package ElementalType;

import Species.Dragon;

//Unfinal
public interface Normal
{
	String _baseType = "Normal";
	
	public void Claw(Dragon _t, Dragon _s);
	public void Takedown(Dragon _t, Dragon _s);
	public void Charge(Dragon _t, Dragon _s);
	public void Wing(Dragon _t, Dragon _s);
	public void Glare(Dragon _t, Dragon _s);
	public void Hit(Dragon _t, Dragon _s);
	public void FrighteningGaze(Dragon _t, Dragon _s);
	public void Tear(Dragon _t, Dragon _s);
	public void Headbutt(Dragon _t, Dragon _s);
	public void Eviscerate(Dragon _t, Dragon _s);
	public void Enrage(Dragon _t, Dragon _s);
	public void FormationCharge(Dragon _t, Dragon _s);
}
