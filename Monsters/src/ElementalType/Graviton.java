package ElementalType;

import Species.Dragon;

public interface Graviton 
{
	String _baseType = "Graviton";

	public void Gravity(Dragon _t, Dragon _s);
	public void Heavy(Dragon _t, Dragon _s);
	public void ImobilePresence(Dragon _t, Dragon _s);
	public void GravitonBeam(Dragon _t, Dragon _s);
	public void Surge(Dragon _t, Dragon _s);
	public void Focus(Dragon _t, Dragon _s);
	public void Cleanse(Dragon _t, Dragon _s);
	public void Eviscerate(Dragon _t, Dragon _s);
	public void Stonewall(Dragon _t, Dragon _s);
	public void ThrowWeight(Dragon _t, Dragon _s);
	public void CoreSurge(Dragon _t, Dragon _s);
	public void MatterAlignment(Dragon _t, Dragon _s);
}
