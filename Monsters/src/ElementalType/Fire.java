package ElementalType;

public interface Fire
{
	String _baseType = "Fire";
	String[] _abilityHeaders = {"Fireball", "Burn", "Engulfing Flames", "Volcano", "Burning Claw", "Temples Calling", "Purity Flame", "Burrow", "Magma Release", "Gods Hand", "A New Fire", "Firestorm"};
}
