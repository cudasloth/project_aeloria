package ElementalType;

import Species.Dragon;

public interface Krilteka //Empire Dragon, think arcadia FF12
{
	String _baseType = "Krilteka";

	public void EmpiresEcho(Dragon _target, Dragon _source);
	public void SoulsOfTheFallen(Dragon _t, Dragon _s);
	public void ForTheEmpire(Dragon _t, Dragon _s);
	public void CallTheCavalry(Dragon _t, Dragon _s);
	public void ArmorBurst(Dragon _t, Dragon _s);
	public void Embolden(Dragon _t, Dragon _s);
	public void BardsSong(Dragon _t, Dragon _s);
	public void Quickening(Dragon _t, Dragon _s);
	public void Organise(Dragon _t, Dragon _s);
	public void CoreSurge(Dragon _t, Dragon _s);
	public void MatterAlignment(Dragon _t, Dragon _s);
	public void SpiritOfTheCapitol(Dragon _t, Dragon _s);
}
