package ElementalType;

import Species.Dragon;

public interface Lunar 
{
	String _baseType = "Lunar";

	public void LunarFlare(Dragon _target, Dragon _source);
	public void LunarCalling(Dragon _t, Dragon _s);
	public void Cure(Dragon _t, Dragon _s);
	public void Flash(Dragon _t, Dragon _s);
	public void FocussedLight(Dragon _t, Dragon _s);
	public void Shinedown(Dragon _t, Dragon _s);
	public void Arcania(Dragon _t, Dragon _s);
	public void Harmony(Dragon _t, Dragon _s);
	public void LunarInterference(Dragon _t, Dragon _s);
	public void HolySpear(Dragon _t, Dragon _s);
	public void LunarEclipse(Dragon _t, Dragon _s);
	public void PlanetaryAlignment(Dragon _t, Dragon _s);
}
