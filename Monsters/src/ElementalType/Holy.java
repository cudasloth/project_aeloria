package ElementalType;

import Species.Dragon;

public interface Holy 
{
	String _baseType = "Holy";

	public void HolyClaw(Dragon _t, Dragon _s);
	public void ArcaneChannel(Dragon _t, Dragon _s);
	public void MythicShift(Dragon _t, Dragon _s);
	public void RebornSpirit(Dragon _t, Dragon _s);
	public void Cleanse(Dragon _t, Dragon _s);
	public void Cure(Dragon _t, Dragon _s);
	public void Life(Dragon _t, Dragon _s);
	public void Eviscerate(Dragon _t, Dragon _s);
	public void HolySpear(Dragon _t, Dragon _s);
	public void LightStorm(Dragon _t, Dragon _s);
	public void Evanescence(Dragon _t, Dragon _s);
	public void PlanetaryAlignment(Dragon _t, Dragon _s);
}
