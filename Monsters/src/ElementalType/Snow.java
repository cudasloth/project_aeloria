package ElementalType;

import Species.Dragon;

//Unfinal
public interface Snow 
{
	String _baseType = "Snow";

	public void AncientVoices(Dragon _target, Dragon _source);
	public void EchoesPast(Dragon _t, Dragon _s);
	public void Apocalypse(Dragon _t, Dragon _s);
	public void AetherStorm(Dragon _t, Dragon _s);
	public void PlaceOfStillness(Dragon _t, Dragon _s);
	public void BlackThunder(Dragon _t, Dragon _s);
	public void Arcania(Dragon _t, Dragon _s);
	public void EternalSoul(Dragon _t, Dragon _s);
	public void Obliteration(Dragon _t, Dragon _s);
	public void CoreSurge(Dragon _t, Dragon _s);
	public void MatterAlignment(Dragon _t, Dragon _s);
	public void PlanetaryAlignment(Dragon _t, Dragon _s);
}
