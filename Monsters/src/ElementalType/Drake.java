package ElementalType;

import Species.Dragon;

public interface Drake
{
	String _baseType = "Drake";
	
	public void Fly(Dragon _t, Dragon _s);
	public void Divebomb(Dragon _t, Dragon _s);
	public void Scout(Dragon _t, Dragon _s);
	public void DropClaw(Dragon _t, Dragon _s);
	public void Bombard(Dragon _t, Dragon _s);
	public void Target(Dragon _t, Dragon _s);
	public void Identify(Dragon _t, Dragon _s);
	public void Bite(Dragon _t, Dragon _s);
	public void SkyCall(Dragon _t, Dragon _s);
	public void GatherSenses(Dragon _t, Dragon _s);
	public void SunBlock(Dragon _t, Dragon _s);
	public void TacticalRush(Dragon _t, Dragon _s);
}
