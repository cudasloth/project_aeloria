package ElementalType;

import Species.Dragon;

public interface Chrono 
{
	String _baseType = "Chrono";

	public void Rewind(Dragon _t, Dragon _s);
	public void QuantumCrack(Dragon _t, Dragon _s);
	public void ChronometricImpulse(Dragon _t, Dragon _s);
	public void CallOfThartan(Dragon _t, Dragon _s);
	public void SpeedBoost(Dragon _t, Dragon _s);
	public void CrystallineManipulation(Dragon _t, Dragon _s);
	public void Aftermath(Dragon _t, Dragon _s);
	public void ChronoBlade(Dragon _t, Dragon _s);
	public void FarCry(Dragon _t, Dragon _s);
	public void Reminiscence(Dragon _t, Dragon _s);
	public void PlaceOfStillness(Dragon _t, Dragon _s);
	public void Stop(Dragon _t, Dragon _s);
}
