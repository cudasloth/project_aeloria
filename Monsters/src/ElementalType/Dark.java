package ElementalType;

import Species.Dragon;

public interface Dark
{
	String _baseType = "Dark";
	
	public void Claw(Dragon _t, Dragon _s);
	public void ShadowBall(Dragon _t, Dragon _s);
	public void DarkeningCurse(Dragon _t, Dragon _s);
	public void Black(Dragon _t, Dragon _s);
	public void ThunderingDarkness(Dragon _t, Dragon _s);
	public void NightCall(Dragon _t, Dragon _s);
	public void Blind(Dragon _t, Dragon _s);
	public void Gouge(Dragon _t, Dragon _s);
	public void ImposingPresence(Dragon _t, Dragon _s);
	public void Hide(Dragon _t, Dragon _s);
	public void DecrepidBlast(Dragon _t, Dragon _s);
	public void BlackThunder(Dragon _t, Dragon _s);
}
