package ElementalType;

import Species.Dragon;

public interface Ice 
{
	String _baseType = "Ice";

	public void Blizzard(Dragon _target, Dragon _source);
	public void Freeze(Dragon _t, Dragon _s);
	public void Snowfall(Dragon _t, Dragon _s);
	public void Icicle(Dragon _t, Dragon _s);
	public void ColdBreath(Dragon _t, Dragon _s);
	public void Snowflake(Dragon _t, Dragon _s);
	public void SoulShatter(Dragon _t, Dragon _s);
	public void Bite(Dragon _t, Dragon _s);
	public void Blind(Dragon _t, Dragon _s);
	public void IceWall(Dragon _t, Dragon _s);
	public void ShivasSong(Dragon _t, Dragon _s);
	public void Whiteout(Dragon _t, Dragon _s);
}
