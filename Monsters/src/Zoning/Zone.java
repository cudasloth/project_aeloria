package Zoning;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import Engine.DragonFarm;
import Engine.EngineBase;
import Species.Dragon;

public abstract class Zone 
{
	protected int _tierLower = 0;
	protected int _tierUpper = 0;
	protected String _type;
	protected Faction _aff;
	protected ArrayList<Dragon> _pop;
	protected ArrayList<String> _popHeadings;
	protected BufferedImage img;
	
	protected void populateZone()
	{
		_pop = DragonFarm.getInstance().getLocalSpecies(_tierLower, _tierUpper);
		for(Dragon _d : _pop)
		{
			_popHeadings.add(_d.getSpecies());
		}
	}
	
	protected boolean setImage(String _s)
	{
		try 
		{
		    img = ImageIO.read(new File(_s));
		} 
		catch (IOException e) 
		{
			if(EngineBase.getDebug())
			{
				System.out.println("Could not load image " + _s + " in zone " + _type);
				System.out.println("Error: " + e.toString());
			}
			return false;
		}
		return true;
	}
	
	public String getType()
	{
		return _type;
	}
	
	public BufferedImage getImage()
	{
		return img;
	}
	
	public Dragon getRandomEncounter()
	{
		return DragonFarm.getInstance().getDragonBySpecies(_pop.get(EngineBase.getInstance().getRandomInt(_pop.size())).getType());
	}
	
	public int getPopSize()
	{
		return _popHeadings.size();
	}
	
	public ArrayList<String> getHeadings()
	{
		return _popHeadings;
	}
	
	public String getHeading(int _i)
	{
		if(_i < _popHeadings.size())
		{
			return _popHeadings.get(_i);
		}
		else
			return ("Out of Bounds: _popHeadings in " + _type);
	}
}
