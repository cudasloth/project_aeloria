package Zoning;

import java.util.ArrayList;

import Species.Dragon;

public class Forest extends Zone
{
	public Forest(int _l, int _u)
	{
		_tierLower = _l;
		_tierUpper = _u;
		_type = "Forest";
		_pop = new ArrayList<Dragon>();
		_popHeadings = new ArrayList<String>();
		setImage("Assets//Forest.jpg");
		populateZone();
	}
}
