package Zoning;

import java.util.ArrayList;

import Species.Dragon;

public class Beach extends Zone 
{
	public Beach(int _l, int _u)
	{
		_tierLower = _l;
		_tierUpper = _u;
		_type = "Beach";
		_pop = new ArrayList<Dragon>();
		_popHeadings = new ArrayList<String>();
		setImage("Assets//Beach.jpg");
		populateZone();
	}
}
