package Zoning;

public class Faction 
{
	private String _fac;
	
	public Faction(String _f)
	{
		_fac = _f;
	}
	
	public String getAffinity()
	{
		return _fac;
	}
}
