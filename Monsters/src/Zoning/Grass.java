package Zoning;

import java.util.ArrayList;

import Species.Dragon;

public class Grass extends Zone 
{
	public Grass(int _l, int _u)
	{
		_tierLower = _l;
		_tierUpper = _u;
		_type = "Grass";
		_pop = new ArrayList<Dragon>();
		_popHeadings = new ArrayList<String>();
		setImage("Assets//Grass.jpg");
		populateZone();
	}
}
